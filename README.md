Что бы не добавлять в каждый пакет файл npmrc нужно прописать следующие в глобавльный npm config заменив <...> на ваши значения:
<scope name> - в моем случае это @ui-kit-advanced
<project id> - можно посмотреть в гитлабе на главной странице репы
<auth token> - нужно будет сгенерировать
учитывайте пробелы тут это примерно npm config set <key> <value>

npm config set <scope name>:registry https://gitlab.com/api/v4/projects/<project id>/packages/npm/

npm config set //gitlab.com/api/v4/projects/<project id>/packages/npm/:_authToken <auth token>