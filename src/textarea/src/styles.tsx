import styled, { css } from 'styled-components';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';

export const StyledTextAreaWrapper = styled.div`
	position: relative;

	> textarea:focus + label,
	> textarea:not(:placeholder-shown) + label {
		top: 0;
		font-size: 1.2rem;
		color: ${({ theme }) => theme.textarea.withValue.placeholder};
		font-weight: 300;
	}

	.iconAfter {
		position: absolute;
		right: 1rem;
		bottom: 1rem;
	}
`;

export const StyledTextArea = styled.textarea<{ isInvalid: boolean }>`
	padding: 1.5rem;
	box-sizing: border-box;
	outline: none;
	min-height: 8rem;
	max-height: 20rem;
	resize: none;
	width: 100%;
	color: ${({ theme }) => theme.textarea.color};
	border: 2px solid ${({ theme }) => theme.textarea.border};
	background: ${({ theme }) => theme.textarea.bg};
	border-radius: 5px;
	transition: all ${ANIMATION_SEC}s ease;
	font: inherit;
	font-size: 1.4rem;
	line-height: 2rem;

	:hover {
		border-color: ${({ theme }) => theme.textarea.hover.border};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.textarea.placeholder};
		}
	}

	:focus {
		border-color: ${({ theme }) => theme.textarea.focus.border};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.textarea.focus.placeholder} !important;
		}
	}

	:disabled {
		color: ${({ theme }) => theme.textarea.disabled.color};
		cursor: no-drop;
		border-color: ${({ theme }) => theme.textarea.disabled.border};
		background: ${({ theme }) => theme.textarea.disabled.bg};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.textarea.disabled.placeholder};
		}
	}

	${({ isInvalid }) =>
		isInvalid &&
		css`
			border-color: ${({ theme }) => theme.textarea.invalid.border};
			background: ${({ theme }) => theme.textarea.invalid.bg};

			// placeholder
			& + * {
				color: ${({ theme }) => theme.textarea.invalid.placeholder} !important;
			}
		`}
`;

export const StyledTextAreaLabel = styled.label<{ isRequired: boolean }>`
	position: absolute;
	white-space: nowrap;
	color: ${({ theme }) => theme.textarea.placeholder};
	font-size: 1.4rem;
	line-height: 2rem;
	pointer-events: none;
	transition: all ${ANIMATION_SEC}s ease;
	top: 1.5rem;
	left: 1.5rem;

	${({ isRequired }) =>
		isRequired &&
		css`
			::before {
				content: '* ';
				color: ${({ theme }) => theme.textarea.requiredColor};
			}
		`}
`;

StyledTextAreaWrapper.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledTextArea.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledTextAreaLabel.defaultProps = { theme: THEME_LIGHT_COLORS };
