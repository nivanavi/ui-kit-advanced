import React, { useCallback } from 'react';
import { Loader } from '@ui-kit-advanced/loader/dist/Loader';
import { TextareaComponent } from './types';
import { StyledTextArea, StyledTextAreaLabel, StyledTextAreaWrapper } from './styles';

let timeout: ReturnType<typeof setTimeout>;

export const Textarea: TextareaComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { isInvalid, isRequired, placeholder, isLoading, isDisabled, onChange, ...rest } = props;

		const debounceMs = 'debounceMs' in props ? props.debounceMs : undefined;

		const onChangeDebounce = useCallback(
			(ev: React.ChangeEvent<HTMLTextAreaElement>): void => {
				if (typeof debounceMs !== 'number') return;
				if (timeout) clearTimeout(timeout);
				timeout = setTimeout(() => {
					onChange?.(ev);
				}, debounceMs);
			},
			[debounceMs, onChange]
		);

		const textareaChangeHandler = useCallback(
			(ev: React.ChangeEvent<HTMLTextAreaElement>): void => {
				if (typeof debounceMs === 'number') return onChangeDebounce(ev);
				onChange?.(ev);
			},
			[onChange, debounceMs, onChangeDebounce]
		);

		return (
			<StyledTextAreaWrapper>
				<StyledTextArea
					{...rest}
					ref={ref}
					onChange={textareaChangeHandler}
					disabled={!!isDisabled}
					placeholder=' '
					isInvalid={!!isInvalid}
				/>
				{isLoading && (
					<span className='iconAfter'>
						<Loader size='m' />
					</span>
				)}
				{!!placeholder && <StyledTextAreaLabel isRequired={!!isRequired}>{placeholder}</StyledTextAreaLabel>}
			</StyledTextAreaWrapper>
		);
	})
);
