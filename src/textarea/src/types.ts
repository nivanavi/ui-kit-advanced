import React from 'react';

type TextareaPropsGeneral = {
	/**
	 * Состояние (ошибка)
	 */
	isInvalid?: boolean;
	/**
	 * Состояние (загрузка)
	 */
	isLoading?: boolean;
	/**
	 * Состояние (заблокирован)
	 */
	isDisabled?: boolean;
	/**
	 * Иконка перед вводимым текстом
	 */
	iconBefore?: React.ReactNode;
	/**
	 * Иконка после вводимого текста
	 */
	iconAfter?: React.ReactNode;
	/**
	 * Красная звездочка для отображения обязательности поля
	 */
	isRequired?: boolean;
} & React.ComponentProps<'textarea'>;

type TextareaPropsDebounce = TextareaPropsGeneral & {
	/**
	 * Время на которое будет дебаунсится вызов функции onChange
	 */
	debounceMs?: number;
	value?: never;
};

type TextareaPropsControlled = TextareaPropsGeneral & {
	debounceMs?: never;
};

export type TextareaProps = TextareaPropsDebounce | TextareaPropsControlled;

export type TextareaComponent = React.ForwardRefExoticComponent<
	TextareaProps & React.RefAttributes<HTMLTextAreaElement>
>;
