import React from 'react';

export type CheckboxProps = {
	/**
	 * Текст справа от чекбокса
	 */
	label?: string;
	/**
	 * Состояние (ошибка)
	 */
	isInvalid?: boolean;
	/**
	 * Состояние (загрузка)
	 */
	isLoading?: boolean;
	/**
	 * Состояние (заблокирован)
	 */
	isDisabled?: boolean;
	/**
	 * Красная звездочка для отображения обязательности поля
	 */
	isRequired?: boolean;
} & React.ComponentProps<'input'>;

export type CheckboxComponent = React.ForwardRefExoticComponent<CheckboxProps & React.RefAttributes<HTMLInputElement>>;
