import styled, { css } from 'styled-components';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';

export const StyledCheckboxWrapper = styled.label<{ isInvalid: boolean }>`
	cursor: pointer;
	white-space: nowrap;
	color: ${({ theme }) => theme.checkbox.color};
	transition: all ${ANIMATION_SEC}s;

	${({ isInvalid }) =>
		isInvalid &&
		css`
			color: ${({ theme }) => theme.checkbox.invalidColor};

			input + * {
				border-color: ${({ theme }) => theme.checkbox.box.invalid.border};
			}
		`};

	:hover {
		color: ${({ theme }) => theme.checkbox.hoverColor};
	}

	:active {
		color: ${({ theme }) => theme.checkbox.activeColor};
	}

	> *:last-child {
		margin-left: 1rem;
	}
`;

export const StyledPseudoCheckbox = styled.span`
	width: 2rem;
	height: 2rem;
	display: inline-flex;
	align-items: center;
	justify-content: center;
	transition: all ${ANIMATION_SEC}s;
	border: 2px solid ${({ theme }) => theme.checkbox.box.border};
	border-radius: 4px;
	background: ${({ theme }) => theme.checkbox.box.bg};
	box-sizing: border-box;

	> svg {
		opacity: 0;
	}
`;

export const StyledCheckbox = styled.input`
  position: absolute;

  width: 1px;
  height: 1px;
  margin: -1px;
  padding: 0;
  overflow: hidden;

  border: 0;

  clip: rect(0 0 0 0);

  :checked {
    // custom checkbox
    & + * {
      border-color: ${({ theme }) => theme.checkbox.box.checked.border};
      background: ${({ theme }) => theme.checkbox.box.checked.bg};
      color: ${({ theme }) => theme.checkbox.box.checked.color};

      svg {
        opacity: 1;
      }
    }
  }

  :disabled {
    // custom checkbox
    & + * {
      cursor: no-drop;
      border-color: ${({ theme }) => theme.checkbox.box.disabled.border};
      background: ${({ theme }) => theme.checkbox.box.disabled.bg};
      color: ${({ theme }) => theme.checkbox.box.disabled.color};
    }

    // label after custom checkbox
    & + * + * {
      color: ${({ theme }) => theme.checkbox.disabledColor};
      cursor: no-drop;
    }
  }

  :required {
    & + * + *::after {
      content: " *";
      color: ${({ theme }) => theme.checkbox.requiredColor};
    }
  }
}
`;

StyledCheckboxWrapper.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledPseudoCheckbox.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledCheckbox.defaultProps = { theme: THEME_LIGHT_COLORS };
