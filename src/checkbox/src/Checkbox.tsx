import React from 'react';
import { CheckboxIcon } from '@ui-kit-advanced/icons';
import { Text } from '@ui-kit-advanced/text';
import { StyledCheckbox, StyledPseudoCheckbox, StyledCheckboxWrapper } from './styles';
import { CheckboxComponent } from './types';

export const Checkbox: CheckboxComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { label, isInvalid, isRequired, isDisabled, ...rest } = props;

		return (
			<StyledCheckboxWrapper isInvalid={!!isInvalid}>
				<StyledCheckbox {...rest} disabled={!!isDisabled} type='checkbox' required={isRequired} ref={ref} />
				<StyledPseudoCheckbox>
					<CheckboxIcon />
				</StyledPseudoCheckbox>
				{label ? <Text>{label}</Text> : null}
			</StyledCheckboxWrapper>
		);
	})
);
