import React from 'react';
import { Input } from '@ui-kit-advanced/input';
import Inputmask from 'inputmask';
import { InputMaskComponent } from './types';

export const InputMask: InputMaskComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { mask, ...rest } = props;

		const setupRefs = React.useCallback(
			(node: HTMLInputElement | null) => {
				if (node) Inputmask({ mask, showMaskOnHover: false, showMaskOnFocus: false }).mask(node);
				if (!ref) return;
				if (typeof ref === 'function') {
					ref(node);
				} else {
					ref.current = node;
				}
			},
			[mask, ref]
		);

		return <Input ref={setupRefs} {...rest} />;
	})
);
