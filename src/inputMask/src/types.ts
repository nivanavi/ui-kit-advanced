import React from 'react';
import { InputProps } from '@ui-kit-advanced/input';

export type InputMaskProps = {
	mask: string;
} & InputProps;

export type InputMaskComponent = React.ForwardRefExoticComponent<
	InputMaskProps & React.RefAttributes<HTMLInputElement>
>;
