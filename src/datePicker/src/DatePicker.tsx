import React from 'react';
import { CSSTransition } from 'react-transition-group';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { ClickAway, useSwitch } from '@ui-kit-advanced/hooks';
import { InputMask } from '@ui-kit-advanced/input-mask';
import { Button } from '@ui-kit-advanced/button';
import { CloseIcon, DatePickerIcon } from '@ui-kit-advanced/icons';
import { DatePickerProps } from './types';
import { StyledDatePickerWrapper } from './styles';

dayjs.extend(customParseFormat);

const FORMAT_DATE = 'DD.MM.YYYY';
const VALID_DATE_LENGTH = 8;

const formatStringRuToDate = (date: string): Date => dayjs(date, FORMAT_DATE).toDate();
const formatDateToStringRu = (date: Date): string => dayjs(date).format(FORMAT_DATE);

export const DatePicker: React.FC<DatePickerProps> = React.memo(props => {
	const { value, defaultValue, minDate, maxDate, onChange, isDisabled, ...rest } = props;
	const [stateDate, setDate] = React.useState<Date | null>(defaultValue || null);
	const [stateIsOpen, switchIsOpen] = useSwitch(false);
	const [stateInputValue, setInputValue] = React.useState<string>('');

	React.useEffect(() => {
		if (value === undefined) return;
		setDate(value);
		if (!value) return;
		setInputValue(formatDateToStringRu(value));
	}, [value]);

	const onChangeHandler = React.useCallback(
		(currentValue: Date | null) => {
			onChange?.(currentValue);
			setDate(currentValue);
			setInputValue(currentValue ? formatDateToStringRu(currentValue) : '');
		},
		[onChange]
	);

	const onInputChangeHandler = React.useCallback(
		(ev: React.ChangeEvent<HTMLInputElement>) => {
			const {
				target: { value: currentValue },
			} = ev;
			setInputValue(currentValue);
			if (!currentValue) return onChangeHandler(null);
			if (currentValue.replace(/[^0-9]/g, '').length !== VALID_DATE_LENGTH) return;
			const stringToDate: Date = formatStringRuToDate(currentValue);
			onChangeHandler(stringToDate);
			if (stateIsOpen) switchIsOpen(false);
		},
		[onChangeHandler, stateIsOpen, switchIsOpen]
	);

	const onCalendarChangeHandler = React.useCallback(
		(currentValue: Date) => {
			onChangeHandler(currentValue);
			switchIsOpen(false);
		},
		[onChangeHandler, switchIsOpen]
	);

	const clearCalendarHandler = React.useCallback(() => onChangeHandler(null), [onChangeHandler]);

	return (
		<ClickAway isListenEscape={stateIsOpen} onClickAway={(): void => switchIsOpen(false)}>
			<StyledDatePickerWrapper>
				<InputMask
					{...rest}
					mask='99.99.9999'
					autoComplete='off'
					value={stateInputValue}
					isDisabled={isDisabled}
					onChange={onInputChangeHandler}
					onClick={(): void => switchIsOpen(true)}
					iconAfter={
						stateDate ? (
							<Button isDisabled={isDisabled} appearance='icon' onClick={clearCalendarHandler} icon={<CloseIcon />} />
						) : (
							<Button isDisabled={isDisabled} appearance='icon' icon={<DatePickerIcon />} />
						)
					}
				/>
				<CSSTransition
					in={stateIsOpen}
					timeout={300}
					mountOnEnter
					unmountOnExit
					classNames={{
						enterActive: 'calendarShow',
						enterDone: 'calendarShow',
						exitActive: 'calendarHide',
						exitDone: 'calendarHide',
					}}
				>
					<Calendar
						next2Label={null}
						prev2Label={null}
						maxDate={maxDate || undefined}
						minDate={minDate || undefined}
						value={stateDate}
						onChange={onCalendarChangeHandler}
					/>
				</CSSTransition>
			</StyledDatePickerWrapper>
		</ClickAway>
	);
});
