import { InputMaskProps } from '@ui-kit-advanced/input-mask';

export type DatePickerProps = {
	onChange?: (value: Date | null) => void;
	defaultValue?: Date | null;
	value?: Date | null;
	minDate?: Date;
	maxDate?: Date;
} & Pick<InputMaskProps, 'isInvalid' | 'isLoading' | 'isDisabled' | 'iconBefore' | 'iconAfter' | 'isRequired'>;
