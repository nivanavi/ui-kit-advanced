import { SelectOptionsProps } from '@ui-kit-advanced/select-options';
import { InputProps } from '@ui-kit-advanced/input';

export type TypesDaData = 'address' | 'party' | 'bank' | 'email' | 'fio' | 'fms_unit';

export type DaDataValue = {
	data?: object;
	unrestricted_value?: string;
	value: string;
};

export type DaDataProps = {
	daDataType: TypesDaData;
	daDataToken: string;
	defaultValue?: DaDataValue;
	value?: DaDataValue;
	onChange?: (value: DaDataValue | null) => void;
	isClearable?: boolean;
} & Omit<SelectOptionsProps<DaDataValue>, 'onChange' | 'value' | 'options'> &
	Pick<InputProps, 'isInvalid' | 'isLoading' | 'isDisabled' | 'iconBefore' | 'iconAfter' | 'isRequired' | 'debounceMs'>;
