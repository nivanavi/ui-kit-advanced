import React, { useCallback } from 'react';
import { CSSTransition } from 'react-transition-group';
import { ClickAway, useSwitch } from '@ui-kit-advanced/hooks';
import { Input } from '@ui-kit-advanced/input';
import { SelectOption, SelectOptions } from '@ui-kit-advanced/select-options';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { Button } from '@ui-kit-advanced/button';
import { CloseIcon } from '@ui-kit-advanced/icons';
import { ConditionalRender } from '@ui-kit-advanced/hooks/dist/components/conditionalRender';
import { DaDataProps, DaDataValue, TypesDaData } from './types';
import { WrapperDaData } from './styles';

let timeout: ReturnType<typeof setTimeout>;

const getDefaultDaData = async (address: string, type: TypesDaData, token: string): Promise<DaDataValue[] | null> => {
	const url = `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/${type}`;
	const options: RequestInit = {
		method: 'POST',
		mode: 'cors',
		headers: {
			'Content-Type': 'application/json',
			Accept: 'application/json',
			Authorization: `Token ${token}`,
		},
		body: JSON.stringify({ query: address }),
	};
	const response = await fetch(url, options);
	if (response.status === 200) {
		const { suggestions } = (await response.json()) || {};
		if (suggestions) return suggestions;
		return null;
	}
	return null;
};

// todo разобраться с анимацией скрытия

export const DaData: React.FC<DaDataProps> = React.memo(props => {
	const {
		daDataType,
		daDataToken,
		value,
		defaultValue,
		emptyMessage,
		dropDownHelpMessage,
		isClearable,
		onChange,
		isLoading,
		isDisabled,
		debounceMs = 500,
		...rest
	} = props;

	const [stateIsOpen, switchIsOpen] = useSwitch(false);

	const [stateInputValue, setInputValue] = React.useState<string>(defaultValue?.value || '');
	const [stateItemsFromDaData, setItemsFromDaData] = React.useState<DaDataValue[] | null>(null);
	const [stateIsSearching, setIsSearching] = React.useState<boolean>(false);

	const options: SelectOption<{ id: string } & DaDataValue>[] = (stateItemsFromDaData || []).map(daDataItem => ({
		label: daDataItem.value,
		value: {
			id: daDataItem.unrestricted_value || '',
			...daDataItem,
		},
	}));
	const selectedOption: SelectOption<{ id: string } & DaDataValue> | undefined = (options || []).find(
		option => stateInputValue === option.label
	);
	const isShowClear: boolean = !!selectedOption && Boolean(isClearable);

	React.useEffect(() => {
		if (value === undefined || value === null) return;
		setInputValue(value.value);
	}, [value]);

	const inputChangeHandler = useCallback(
		({ target: { value: currentInputValue } }: React.ChangeEvent<HTMLInputElement>): void => {
			if (!stateIsOpen) switchIsOpen(true);
			setInputValue(currentInputValue);
			onChange?.(currentInputValue ? { value: currentInputValue } : null);
			if (timeout) clearTimeout(timeout);
			timeout = setTimeout(() => {
				if (!currentInputValue) return;
				setIsSearching(true);
				getDefaultDaData(currentInputValue, daDataType, daDataToken)
					.then((data: DaDataValue[] | null) => {
						if (!data) return;
						setItemsFromDaData(data);
					})
					.finally(() => setIsSearching(false));
			}, debounceMs);
		},
		[stateIsOpen, switchIsOpen, onChange, debounceMs, daDataType, daDataToken]
	);

	const clearHandler = useCallback((): void => setInputValue(''), []);

	const daDataChangeHandler = useCallback(
		(selectOption: SelectOption<DaDataValue>): void => {
			const currentValue: DaDataValue = selectOption.value;
			setInputValue(selectOption.label);
			switchIsOpen(false);
			onChange?.(currentValue);
		},
		[switchIsOpen, onChange]
	);

	return (
		<ClickAway isListenEscape={stateIsOpen} onClickAway={(): void => switchIsOpen(false)}>
			<WrapperDaData>
				<Input
					{...rest}
					onClick={(): void => switchIsOpen()}
					isDisabled={isDisabled}
					isLoading={isLoading || stateIsSearching}
					onChange={inputChangeHandler}
					value={stateInputValue}
					iconAfter={
						<ConditionalRender isRender={isShowClear}>
							<Button onClick={(): void => clearHandler()} appearance='icon' icon={<CloseIcon />} />
						</ConditionalRender>
					}
				/>
				<CSSTransition
					in={stateIsOpen}
					timeout={ANIMATION_SEC * 1000}
					mountOnEnter
					unmountOnExit
					classNames={{
						enterActive: 'optionsShow',
						enterDone: 'optionsShow',
						exitActive: 'optionsHide',
						exitDone: 'optionsHide',
					}}
				>
					<SelectOptions
						isLoading={isLoading || stateIsSearching}
						highlightText={stateInputValue}
						options={options}
						emptyMessage={emptyMessage}
						value={selectedOption}
						onChange={daDataChangeHandler}
						dropDownHelpMessage={dropDownHelpMessage}
					/>
				</CSSTransition>
			</WrapperDaData>
		</ClickAway>
	);
});
