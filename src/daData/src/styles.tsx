import styled from 'styled-components';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';

export const WrapperDaData = styled.div`
	position: relative;

	.optionsShow {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: unset;
		opacity: 1;
	}

	.optionsHide {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: none;
		opacity: 0;
	}
`;
