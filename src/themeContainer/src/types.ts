import React from 'react';

export type ThemeContainerProps = {
	children: React.ReactNode;
};

export type ThemeContextType = {
	toggleTheme: () => void;
};

export enum ThemeEnum {
	LIGHT_THEME = 'LIGHT_THEME',
	DARK_THEME = 'DARK_THEME',
}
