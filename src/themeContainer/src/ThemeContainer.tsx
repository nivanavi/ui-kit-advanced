import React, { useState } from 'react';
import { ThemeProvider } from 'styled-components';
import { THEME_DARK_COLORS, THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';
import { ThemeContainerProps, ThemeContextType, ThemeEnum } from './types';

export const ThemeContext = React.createContext({} as ThemeContextType);

const { LIGHT_THEME, DARK_THEME } = ThemeEnum;

export const ThemeContainer: React.FC<ThemeContainerProps> = React.memo(props => {
	const { children } = props;
	const [stateTheme, setTheme] = useState<ThemeEnum>(LIGHT_THEME);
	const themeMode = stateTheme === LIGHT_THEME ? THEME_LIGHT_COLORS : THEME_DARK_COLORS;

	const contextValue: ThemeContextType = React.useMemo(
		() => ({
			toggleTheme: (): void => setTheme(currentTheme => (currentTheme === LIGHT_THEME ? DARK_THEME : LIGHT_THEME)),
		}),
		[]
	);

	return (
		<ThemeProvider theme={themeMode}>
			<ThemeContext.Provider value={contextValue}>{children}</ThemeContext.Provider>
		</ThemeProvider>
	);
});
