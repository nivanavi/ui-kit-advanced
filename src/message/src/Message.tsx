import React from 'react';
import { TextS } from '@ui-kit-advanced/text';
import { StyledFormMessage } from './styles';
import { MessageProps } from './types';

export const Message: React.FC<MessageProps> = React.memo(props => {
	const { message, type } = props;

	if (!message) return null;

	return (
		<StyledFormMessage type={type}>
			<TextS>{message}</TextS>
		</StyledFormMessage>
	);
});
