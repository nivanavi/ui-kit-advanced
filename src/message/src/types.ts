export type MessageTypes = 'error' | 'success' | 'hint';

export type MessageProps = {
	message?: string;
	type: MessageTypes;
};
