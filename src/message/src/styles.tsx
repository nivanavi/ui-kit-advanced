import styled from 'styled-components';
import { MessageTypes } from './types';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';

export const StyledFormMessage = styled.div<{ type: MessageTypes }>`
	position: absolute;
	${({ type, theme }) => {
		switch (type) {
			case 'error':
				return `color: ${theme.message.errorColor}`;
			case 'hint':
				return `color: ${theme.message.hintColor}`;
			case 'success':
				return `color: ${theme.message.successColor}`;
			default:
				return `color: ${theme.message.defaultColor}`;
		}
	}}
`;

StyledFormMessage.defaultProps = { theme: THEME_LIGHT_COLORS };
