import React, { ReactNode } from 'react';

export type TooltipProps = {
	tooltip?: string;
};

export type TypeButtonStatus = 'default' | 'success' | 'info' | 'warning' | 'danger' | 'common';

type ButtonGeneralType = {
	isLoading?: boolean;
	isDisabled?: boolean;
	tooltip?: string;
	status?: TypeButtonStatus;
} & React.ComponentProps<'button'>;

type ButtonIconType = {
	iconBefore?: ReactNode;
	iconAfter?: ReactNode;
};

type FilledButtonType = {
	appearance: 'filled';
} & ButtonGeneralType &
	ButtonIconType;

type TransparentButtonType = {
	appearance: 'transparent';
} & ButtonGeneralType &
	ButtonIconType;

type LinkButtonType = {
	appearance: 'link';
} & ButtonGeneralType &
	ButtonIconType;

type TextButtonType = {
	appearance: 'text';
} & ButtonGeneralType &
	ButtonIconType;

type IconButtonType = {
	appearance: 'icon';
	icon: ReactNode;
} & ButtonGeneralType;

type FlagButtonType = {
	appearance: 'flag';
	icon: ReactNode;
	isSelected?: boolean;
} & ButtonGeneralType;

export type ButtonProps =
	| FilledButtonType
	| TransparentButtonType
	| LinkButtonType
	| TextButtonType
	| IconButtonType
	| FlagButtonType;

export type ButtonComponent = React.ForwardRefExoticComponent<ButtonProps & React.RefAttributes<HTMLButtonElement>>;
