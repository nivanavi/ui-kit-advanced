import styled, { css } from 'styled-components';
import { TypeButtonStatus } from './types';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';
import { ANIMATION_SEC, Z_INDEX_MODAL } from '@ui-kit-advanced/consts';

const defaultIconsStyles = css`
	.iconBefore {
		display: block;
		margin-right: 1.2rem;
	}

	.iconAfter {
		display: block;
		margin-left: 1.2rem;
	}
`;

const defaultButtonsStyles = css<{ isLoading: boolean }>`
	display: flex;
	align-items: center;
	position: relative;
	cursor: pointer;

	> span {
		opacity: ${({ isLoading }) => (isLoading ? '0' : '1')};
	}

	:hover > .buttonTooltip {
		opacity: 1 !important;
	}
`;

export const StyledFilledButton = styled.button<{ isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	${defaultIconsStyles};
	justify-content: center;
	padding: 0 3.5rem;
	width: 100%;
	border-radius: 0.7rem;
	height: 4rem;
	color: ${({ theme, status }) => theme.button[status].filled.color};
	transition: all ${ANIMATION_SEC}s;
	background: ${({ theme, status }) => theme.button[status].filled.bg};
	border: 1px solid ${({ theme, status }) => theme.button[status].filled.border};
	font: inherit;

	:hover,
	:focus {
		background: ${({ theme, status }) => theme.button[status].filled.hover.bg};
		border: 1px solid ${({ theme, status }) => theme.button[status].filled.hover.border};
	}

	:active {
		background: ${({ theme, status }) => theme.button[status].filled.active.bg};
		border: 1px solid ${({ theme, status }) => theme.button[status].filled.active.border};
	}

	:disabled {
		background: ${({ theme, status }) => theme.button[status].filled.disabled.bg};
		border: 1px solid ${({ theme, status }) => theme.button[status].filled.disabled.border};
		color: ${({ theme, status }) => theme.button[status].filled.disabled.color};
		cursor: no-drop;
	}
`;

export const StyledTransparentButton = styled.button<{ isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	${defaultIconsStyles};
	justify-content: center;
	padding: 0 3.5rem;
	width: 100%;
	border-radius: 0.7rem;
	height: 4rem;
	box-sizing: border-box;
	color: ${({ theme, status }) => theme.button[status].transparent.color};
	transition: all ${ANIMATION_SEC}s;
	background: ${({ theme, status }) => theme.button[status].transparent.bg};
	border: 2px solid ${({ theme, status }) => theme.button[status].transparent.border};
	font-weight: 500;

	:hover,
	:focus {
		background: ${({ theme, status }) => theme.button[status].transparent.hover.bg};
		color: ${({ theme, status }) => theme.button[status].transparent.hover.color};
	}

	:active {
		background: ${({ theme, status }) => theme.button[status].transparent.active.bg};
		border: 2px solid ${({ theme, status }) => theme.button[status].transparent.active.border};
		color: ${({ theme, status }) => theme.button[status].transparent.active.color};
	}

	:disabled {
		background: ${({ theme, status }) => theme.button[status].transparent.disabled.bg};
		cursor: no-drop;
		color: ${({ theme, status }) => theme.button[status].transparent.disabled.color};
		border: 2px solid ${({ theme, status }) => theme.button[status].transparent.disabled.border};
	}
`;

export const StyledLinkButton = styled.button<{ isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	${defaultIconsStyles};
	border: unset;
	background: unset;
	transition: all ${ANIMATION_SEC}s;
	color: ${({ theme, status }) => theme.button[status].link.color};
	font-weight: 500;

	> span {
		text-decoration: underline;
	}

	:hover {
		color: ${({ theme, status }) => theme.button[status].link.hover.color};
	}

	:active {
		color: ${({ theme, status }) => theme.button[status].link.active.color};
	}

	:disabled {
		cursor: no-drop;
		color: ${({ theme, status }) => theme.button[status].link.disabled.color};
	}
`;

export const StyledTextButton = styled.button<{ isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	${defaultIconsStyles};
	border: unset;
	background: unset;
	transition: all ${ANIMATION_SEC}s;
	color: ${({ theme, status }) => theme.button[status].text.color};
	font-weight: 500;

	:hover {
		color: ${({ theme, status }) => theme.button[status].text.hover.color};
	}

	:active {
		color: ${({ theme, status }) => theme.button[status].text.active.color};
	}

	:disabled {
		cursor: no-drop;
		color: ${({ theme, status }) => theme.button[status].text.disabled.color};
	}
`;

export const StyledIconButton = styled.button<{ isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	border: unset;
	background: unset;
	transition: all ${ANIMATION_SEC}s;
	color: ${({ theme, status }) => theme.button[status].icon.color};

	svg {
		transition: all ${ANIMATION_SEC}s;
	}

	:hover {
		color: ${({ theme, status }) => theme.button[status].icon.hover.color};
	}

	:active {
		color: ${({ theme, status }) => theme.button[status].icon.active.color};
	}

	:disabled {
		cursor: no-drop;
		color: ${({ theme, status }) => theme.button[status].icon.disabled.color};
	}
`;
export const StyledFlagButton = styled.button<{ isSelected: boolean; isLoading: boolean; status: TypeButtonStatus }>`
	${defaultButtonsStyles};
	width: 4rem;
	height: 4rem;
	border: unset;
	justify-content: center;
	transition: all ${ANIMATION_SEC}s;
	color: ${({ theme, status }) => theme.button[status].flag.color};
	background: ${({ theme, status }) => theme.button[status].flag.bg};

	svg {
		transition: all ${ANIMATION_SEC}s;
	}

	${({ isSelected, status }) =>
		isSelected
			? css`
					background: ${({ theme }) => theme.button[status].flag.selected.bg};
					color: ${({ theme }) => theme.button[status].flag.selected.color};
			  `
			: css`
					:hover {
						background: ${({ theme }) => theme.button[status].flag.hover.bg};
						color: ${({ theme }) => theme.button[status].flag.hover.color};

						svg {
							transform: scale(1.1);
						}
					}

					:active {
						background: ${({ theme }) => theme.button[status].flag.active.bg};
						color: ${({ theme }) => theme.button[status].flag.active.color};

						svg {
							transform: scale(1.05);
						}
					}
			  `};

	:disabled {
		cursor: no-drop;
		color: ${({ theme, status }) => theme.button[status].flag.disabled.color};
		background: ${({ theme, status }) => theme.button[status].flag.disabled.bg};

		svg {
			transform: none;
		}
	}
`;

export const StyledButtonTooltip = styled.span`
	pointer-events: none;
	z-index: ${Z_INDEX_MODAL};
	text-decoration: unset !important;
	opacity: 0 !important;
	top: calc(100% + 0.5rem);
	position: absolute;
	padding: 2px 0.5rem;
	background: ${({ theme }) => theme.tooltip.bg};
	color: ${({ theme }) => theme.tooltip.color};
	left: 50%;
	transform: translate(-50%);
	transition: opacity ${ANIMATION_SEC}s;
	border-radius: 0.5rem;
	p {
		white-space: nowrap;
	}
`;

StyledFilledButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledTransparentButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledLinkButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledTextButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledIconButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledFlagButton.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledButtonTooltip.defaultProps = { theme: THEME_LIGHT_COLORS };
