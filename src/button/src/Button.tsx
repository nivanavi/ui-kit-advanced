import React, { MouseEvent } from 'react';
import { TextS, Text } from '@ui-kit-advanced/text';
import { Loader } from '@ui-kit-advanced/loader/dist/Loader';
import {
	StyledFilledButton,
	StyledTransparentButton,
	StyledLinkButton,
	StyledTextButton,
	StyledIconButton,
	StyledFlagButton,
	StyledButtonTooltip,
} from './styles';
import { ButtonComponent, TooltipProps } from './types';

const Tooltip: React.FC<TooltipProps> = props => {
	const { tooltip } = props;
	if (!tooltip) return null;

	return (
		<StyledButtonTooltip className='buttonTooltip'>
			<TextS>{tooltip}</TextS>
		</StyledButtonTooltip>
	);
};

export const Button: ButtonComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { appearance, status = 'default', isLoading, isDisabled, type, onClick, tooltip, children, ...rest } = props;

		const clickHandler = React.useCallback(
			(ev: MouseEvent<HTMLButtonElement>): void => {
				if (isDisabled) return ev.preventDefault();
				onClick?.(ev);
			},
			[isDisabled, onClick]
		);

		const loader = React.useMemo(() => (isLoading ? <Loader /> : null), [isLoading]);

		const buttonProps = React.useMemo(
			() => ({
				...rest,
				ref,
				type: type || 'button',
				status,
				isLoading: !!isLoading,
				disabled: !!isDisabled || !!isLoading,
				className: `${appearance}Button`,
				onClick: clickHandler,
			}),
			[appearance, clickHandler, isDisabled, isLoading, ref, rest, status, type]
		);

		switch (appearance) {
			case 'filled':
				return (
					<StyledFilledButton {...buttonProps}>
						{loader}
						<Tooltip tooltip={tooltip} />
						{props.iconBefore ? <span className='iconBefore'>{props.iconBefore}</span> : null}
						<Text>{children}</Text>
						{props.iconAfter ? <span className='iconAfter'>{props.iconAfter}</span> : null}
					</StyledFilledButton>
				);
			case 'transparent':
				return (
					<StyledTransparentButton {...buttonProps}>
						{loader}
						<Tooltip tooltip={tooltip} />
						{props.iconBefore ? <span className='iconBefore'>{props.iconBefore}</span> : null}
						<Text>{children}</Text>
						{props.iconAfter ? <span className='iconAfter'>{props.iconAfter}</span> : null}
					</StyledTransparentButton>
				);
			case 'link':
				return (
					<StyledLinkButton {...buttonProps}>
						{loader}
						<Tooltip tooltip={tooltip} />
						{props.iconBefore ? <span className='iconBefore'>{props.iconBefore}</span> : null}
						<Text>{children}</Text>
						{props.iconAfter ? <span className='iconAfter'>{props.iconAfter}</span> : null}
					</StyledLinkButton>
				);
			case 'text':
				return (
					<StyledTextButton {...buttonProps}>
						{loader}
						<Tooltip tooltip={tooltip} />
						{props.iconBefore ? <span className='iconBefore'>{props.iconBefore}</span> : null}
						<Text>{children}</Text>
						{props.iconAfter ? <span className='iconAfter'>{props.iconAfter}</span> : null}
					</StyledTextButton>
				);
			case 'icon':
				return (
					<StyledIconButton {...buttonProps}>
						{loader}
						<Tooltip tooltip={tooltip} />
						<span className='buttonIcon'>{props.icon}</span>
					</StyledIconButton>
				);
			case 'flag':
				return (
					<StyledFlagButton {...buttonProps} isSelected={!!props.isSelected}>
						{loader}
						<Tooltip tooltip={tooltip} />
						<span className='buttonIcon'>{props.icon}</span>
					</StyledFlagButton>
				);
			default:
				return null;
		}
	})
);
