import React, { createElement } from 'react';
import highlightWords from 'highlight-words';
import { HighlightTextProps } from './types';

/**
 * Компонент принимающий текст и строку которую нужно выделить в этом тексте и оборачивает ее в span className: 'highlight'
 */
export const HighlightText: React.FC<HighlightTextProps> = props => {
	const { text, query } = props;

	const chunks = highlightWords({
		text,
		query,
	});

	return createElement(
		'span',
		{},
		chunks.map(chunk => {
			if (chunk.match) return createElement('mark', { className: 'highlight', key: chunk.key }, chunk.text);
			return createElement('span', { key: chunk.key }, chunk.text);
		})
	);
};
