import styled from 'styled-components';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';

// todo убрать transition: opacity ${ANIMATION_SEC}s;

export const StyledSelectItemsWrapper = styled.div<{ dropDownHelpMessage?: string }>`
	position: absolute;
	width: 100%;
	background: ${({ theme }) => theme.selectOptions.bg};
	box-sizing: border-box;
	border: 2px solid ${({ theme }) => theme.selectOptions.border};
	border-radius: 5px;
	margin-top: -2px;
	padding-top: 0.5rem;
	overflow-y: auto;
	max-height: 30rem;
	&:after {
		content: '${({ dropDownHelpMessage }) =>
			dropDownHelpMessage || 'Для навигации можно использовать \u2191, \u2193 и Enter'}';
		position: sticky;
		bottom: 0;
		padding: 5px 0 5px 2rem;
		font-weight: bold;
		font-size: 1.3rem;
		background-color: ${({ theme }) => theme.selectOptions.dropDownHelpMessage.bg};
		display: block;
		width: 100%;
		box-sizing: border-box;
	}

	.highlight {
		background-color: ${({ theme }) => theme.selectOptions.highlightColor};
	}
`;

export const StyledSelectItem = styled.div<{ isSelected: boolean }>`
	padding: 1.5rem 3rem;
	display: flex;
	align-items: center;
	justify-content: flex-start;
	cursor: pointer;
	color: ${({ isSelected, theme }) =>
		isSelected ? theme.selectOptions.item.selectedColor : theme.selectOptions.item.color};

	:focus {
		background: ${({ theme }) => theme.selectOptions.item.hoverBg};
		color: ${({ theme }) => theme.selectOptions.item.hoverColor};
	}

	:hover {
		background: ${({ theme }) => theme.selectOptions.item.hoverBg};
		color: ${({ theme }) => theme.selectOptions.item.hoverColor};
	}
`;

export const StyledSelectEmpty = styled.div`
	padding: 1rem;
	display: flex;
	justify-content: center;
`;

StyledSelectItemsWrapper.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledSelectItem.defaultProps = { theme: THEME_LIGHT_COLORS };
