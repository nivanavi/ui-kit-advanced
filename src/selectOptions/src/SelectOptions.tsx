import React from 'react';
import { Loader } from '@ui-kit-advanced/loader/dist/Loader';
import { Heading4, Text } from '@ui-kit-advanced/text';
import { ClickAway } from '@ui-kit-advanced/hooks';
import { KeysCode } from '@ui-kit-advanced/enums';
import { ConditionalRender } from '@ui-kit-advanced/hooks/dist/components/conditionalRender';
import { SelectOption, SelectOptionsProps } from './types';
import { StyledSelectEmpty, StyledSelectItem, StyledSelectItemsWrapper } from './styles';
import { HighlightText } from './HighlightText';

const SelectOptionsComponent = <T extends { id: string }>(props: SelectOptionsProps<T>): JSX.Element => {
	const {
		dropDownHelpMessage,
		isLoading,
		emptyMessage = 'Нет элементов',
		options,
		onChange,
		value,
		highlightText = '',
	} = props;
	const refDropDownContent = React.useRef<HTMLDivElement | null>(null);
	const refSelectedIndex = React.useRef<number>(-1);
	const refIsTouched = React.useRef<boolean>(false);

	const onChangeHandler = React.useCallback((item: SelectOption<T>): void => onChange?.(item), [onChange]);

	const resetSelectedHandler = React.useCallback((): void => {
		refSelectedIndex.current = 0;
		refIsTouched.current = false;
	}, []);

	const changeSelectedIndexHandler = React.useCallback(
		(index: number): void => {
			if (isLoading) return;
			refSelectedIndex.current = index;
			refIsTouched.current = true;
			if (!refDropDownContent.current?.children) return;
			const selectedElement = refDropDownContent.current.children.item(refSelectedIndex.current);
			if (!selectedElement) return;
			(selectedElement as HTMLDivElement).focus();
		},
		[isLoading]
	);

	const scrollToSelected = React.useCallback((): void => {
		if (!refDropDownContent.current?.children) return;
		const selectedElement = refDropDownContent.current.children.item(refSelectedIndex.current);

		if (!selectedElement) return;
		selectedElement.scrollIntoView({ block: 'center', inline: 'center', behavior: 'smooth' });
	}, []);

	const keyboardEventsHandler = React.useCallback(
		(ev: KeyboardEvent): void => {
			if (!refIsTouched.current) return changeSelectedIndexHandler(0);

			if (ev.key === KeysCode.Enter) {
				return onChangeHandler(options[refSelectedIndex.current]);
			}

			if (ev.key === KeysCode.ArrowUp && refSelectedIndex.current !== 0) {
				changeSelectedIndexHandler(refSelectedIndex.current - 1);
				return scrollToSelected();
			}

			if (ev.key === KeysCode.ArrowDown && refSelectedIndex.current !== options.length - 1) {
				changeSelectedIndexHandler(refSelectedIndex.current + 1);
				return scrollToSelected();
			}
		},
		[changeSelectedIndexHandler, onChangeHandler, options, scrollToSelected]
	);

	React.useEffect(() => {
		document.addEventListener('keyup', keyboardEventsHandler);

		return () => {
			document.removeEventListener('keyup', keyboardEventsHandler);
		};
	}, [keyboardEventsHandler]);

	return (
		<ClickAway onClickAway={resetSelectedHandler}>
			<StyledSelectItemsWrapper dropDownHelpMessage={dropDownHelpMessage} ref={refDropDownContent}>
				<ConditionalRender isRender={Boolean(isLoading)}>
					<Loader />
				</ConditionalRender>
				<ConditionalRender isRender={!isLoading && !!options.length}>
					{options.map(item => (
						<StyledSelectItem
							key={item.value.id}
							tabIndex={-1}
							onClick={(): void => onChangeHandler(item)}
							isSelected={item.value.id === value?.value?.id}
						>
							<Text>
								<HighlightText query={highlightText} text={item.label} />
							</Text>
						</StyledSelectItem>
					))}
				</ConditionalRender>
				<ConditionalRender isRender={!isLoading && !options.length}>
					<StyledSelectEmpty>
						<Heading4>{emptyMessage}</Heading4>
					</StyledSelectEmpty>
				</ConditionalRender>
			</StyledSelectItemsWrapper>
		</ClickAway>
	);
};

export const SelectOptions = React.memo(SelectOptionsComponent) as typeof SelectOptionsComponent;
