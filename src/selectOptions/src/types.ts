export type SelectOption<T> = {
	/**
	 * Текст который будет отображаться в списке
	 */
	label: string;
	/**
	 * Любое значение которые вы хотите хранить etc
	 */
	value: T;
};

export type SelectOptionsProps<T> = {
	/**
	 * Состояние загрузки
	 */
	isLoading?: boolean;
	/**
	 * Список элементов для отображения
	 */
	options: SelectOption<T>[];
	/**
	 * Если хотите контролировать текущее выбранное значение
	 */
	value?: SelectOption<T> | null;
	/**
	 * Текст который нужно выделить (к примеру при поиске)
	 */
	highlightText?: string;
	/**
	 * Сообщение которое отображается если массив опций пустой
	 */
	emptyMessage?: string;
	/**
	 * Коллбэк вызываемый при выборе опции
	 */
	onChange?: (option: SelectOption<T>) => void;
	/**
	 * Сообщение для подсказки пользователю как управлять списком с клавиатуры (или любое другое сообщение)
	 */
	dropDownHelpMessage?: string;
};

export type HighlightTextProps = {
	/**
	 * Полный текст по котрому ведется поиск
	 */
	text: string;
	/**
	 * Текст который нужно найти в исходном тексте
	 */
	query: string;
};
