import styled from 'styled-components';
// heading
export const Heading1 = styled.h1`
	font-family: 'Futura PT Demi', sans-serif;
	font-size: 4rem;
	line-height: 5rem;
	font-weight: 600;
	@media screen and (max-width: 767px) {
		font-size: 2.8rem;
		line-height: 3.5rem;
	}
`;
export const Heading2 = styled.h2`
	font-family: 'Futura PT Demi', serif;
	font-size: 3.2rem;
	line-height: 4rem;
	font-weight: 600;
	@media screen and (max-width: 767px) {
		font-size: 2.2rem;
		line-height: 3rem;
	}
`;
export const Heading3 = styled.h3`
	font-family: 'Futura PT Demi', serif;
	font-size: 2.4rem;
	line-height: 3rem;
	font-weight: 600;
`;
export const Heading4 = styled.h4`
	font-family: 'Verdana', sans-serif;
	font-weight: bold;
	font-size: 1.6rem;
	line-height: 2.4rem;
`;
export const Heading5 = styled.h5`
	font-family: 'Verdana', sans-serif;
	font-weight: bold;
	font-size: 1.4rem;
	line-height: 2rem;
`;

// text
export const TextXXL = styled.p`
	font-family: 'Verdana', sans-serif;
	font-size: 2rem;
	line-height: 3.2rem;
`;
export const TextXl = styled.p`
	font-family: 'Verdana', sans-serif;
	font-size: 1.6rem;
	line-height: 2.4rem;
`;
export const TextL = styled.p`
	font-family: 'Verdana', sans-serif;
	font-size: 1.4rem;
	line-height: 2.2rem;
`;
export const TextM = styled.p`
	font-family: 'Verdana', sans-serif;
	font-size: 1.2rem;
	line-height: 2rem;
	white-space: pre-line;
	margin: 0;
`;
export const TextS = styled.p`
	font-family: 'Verdana', sans-serif;
	font-size: 1rem;
	line-height: 1.5rem;
	margin: 0;
`;
export const TextSubtitle = styled.p`
	font-family: 'Verdana', sans-serif;
	font-weight: bold;
	font-size: 1.1rem;
	line-height: 1.5rem;
`;
export const Text = styled.span`
	font-family: 'Verdana', sans-serif;
	font-size: 1.4rem;
	line-height: 2rem;
`;
