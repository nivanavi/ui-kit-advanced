import React, { cloneElement } from 'react';
import { ConditionalRenderComponent } from './types';

/**
 * Компонент для условного рендера что бы избравиться от тернарных операторов в jsx
 */
export const ConditionalRender: ConditionalRenderComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { isRender, children } = props;

		if (!isRender) return null;

		return React.Children.only(cloneElement(children, { ref }));
	})
);
