import React, { ReactElement } from 'react';

export type ConditionalRenderProps = {
	/**
	 * флаг отвечающий за то будут рендерится children или нет
	 */
	isRender: boolean;
	children: ReactElement;
};

export type ConditionalRenderComponent = React.ForwardRefExoticComponent<ConditionalRenderProps>;
