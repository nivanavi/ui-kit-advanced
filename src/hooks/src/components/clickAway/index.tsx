import React, { cloneElement, MutableRefObject, RefCallback } from 'react';
import { KeysCode } from '@ui-kit-advanced/enums';
import { ClickAwayProps } from './types';
import { useCombinedRefs } from '../../hooks/useCombinedRefs';
import { useOutsideClick } from '../../hooks/useOutsideClick';

/**
 * Передавайте в качестве children контейнер (React.Fragment - нельзя) при клике вне которого вы ожидаете вызов функции onClickAway
 */

// React.useEffect(() => {
// 	if (!refChildren.current) return;
// 	// ждем queueMacrotask()
// 	setTimeout(() => {
// 		refActivated.current = true;
// 	});
//
// 	return () => {
// 		refActivated.current = false;
// 	};
// }, []);

// React.useEffect(() => {
// 	if (!refChildren.current) return;
// 	console.log('add list');
// 	document.addEventListener('click', clickHandler);
//
// 	return (): void => {
// 		console.log('remove list');
// 		document.removeEventListener('click', clickHandler);
// 	};
// });

export const ClickAway: React.FC<ClickAwayProps> = React.memo(props => {
	const { children, onClickAway, closeOnPress } = props;

	const { ref: refChildren } = useOutsideClick(onClickAway);

	const ref = (
		children as typeof children & {
			ref: RefCallback<HTMLElement> | MutableRefObject<HTMLElement>;
		}
	)?.ref;

	const combinedRefs = useCombinedRefs<HTMLElement | null>(refChildren, ref);

	const pressHandler = React.useCallback(
		(ev: KeyboardEvent) => {
			if (Array.isArray(closeOnPress) && closeOnPress.includes(ev.key as KeysCode)) return onClickAway(ev);
			if (ev.key === closeOnPress) onClickAway(ev);
		},
		[closeOnPress, onClickAway]
	);

	React.useEffect(() => {
		if (closeOnPress) {
			document.addEventListener('keyup', pressHandler);
		}
		if (!closeOnPress) {
			document.removeEventListener('keyup', pressHandler);
		}
		return (): void => {
			document.removeEventListener('keyup', pressHandler);
		};
	}, [closeOnPress, pressHandler]);

	if (!children) return null;

	return React.Children.only(cloneElement(children, { ref: combinedRefs }));
});
