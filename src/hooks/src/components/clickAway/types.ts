import { ReactElement } from 'react';
import { KeysCode } from '@ui-kit-advanced/enums';

// todo прописать что я мемоизирую а что нет

export type ClickAwayProps = {
	onClickAway: (ev: MouseEvent | KeyboardEvent) => void;
	closeOnPress?: KeysCode | KeysCode[];
	children?: ReactElement | null;
};
