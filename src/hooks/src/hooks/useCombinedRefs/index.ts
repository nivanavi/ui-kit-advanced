import { useCallback, MutableRefObject, RefCallback } from 'react';

export const useCombinedRefs = <T>(...refs: (MutableRefObject<T> | RefCallback<T>)[]): RefCallback<T> =>
	useCallback(
		(element: T) => {
			refs.forEach(ref => {
				if (!ref) return;
				if (typeof ref === 'function') {
					ref(element);
				} else {
					ref.current = element;
				}
			});
		},
		[refs]
	);
