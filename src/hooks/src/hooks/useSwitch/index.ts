import React from 'react';

export const useSwitch = (defaultValue = false): [boolean, (value?: boolean) => void] => {
	const [stateSwitch, setSwitch] = React.useState<boolean>(defaultValue);

	const switchHandler = React.useCallback((value?: boolean) => {
		if (value === undefined) return setSwitch(state => !state);
		setSwitch(value);
	}, []);

	return [stateSwitch, switchHandler];
};
