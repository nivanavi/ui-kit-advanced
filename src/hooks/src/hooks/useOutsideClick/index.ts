import React, { useRef } from 'react';
import { useLatest } from '../useLatest';

export const useOutsideClick = (onClickAway: (ev: MouseEvent) => void): { ref: React.RefObject<HTMLElement> } => {
	const ref = useRef<HTMLElement>(null);

	const latestOnClickAway = useLatest(onClickAway);

	React.useEffect(() => {
		const clickHandler = (ev: MouseEvent): void => {
			if (!ref.current) return;
			if (!ref.current.contains(ev.target as Node)) latestOnClickAway.current(ev);
		};

		document.addEventListener('click', clickHandler, true);

		return (): void => {
			document.removeEventListener('click', clickHandler, true);
		};
	}, [latestOnClickAway, onClickAway]);

	return {
		ref,
	};
};
