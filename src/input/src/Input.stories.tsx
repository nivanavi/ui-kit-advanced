import React from 'react';
import { Input } from './index';

export default {
	title: 'Input',
	component: Input,
};

export const Default: React.FC = () => <Input />;
