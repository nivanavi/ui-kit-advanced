import React, { useCallback } from 'react';
import { Loader } from '@ui-kit-advanced/loader/dist/Loader';
import { StyledInput, StyledInputLabel, StyledInputWrapper } from './styles';
import { InputComponent, InputStylesProps } from './types';

let timeout: ReturnType<typeof setTimeout>;

export const Input: InputComponent = React.memo(
	React.forwardRef((props, ref) => {
		const { placeholder, isInvalid, isLoading, isDisabled, iconBefore, iconAfter, isRequired, onChange, ...rest } =
			props;

		const debounceMs = 'debounceMs' in props ? props.debounceMs : undefined;

		const stylesProps: InputStylesProps = {
			iconAfter: !!iconAfter,
			iconBefore: !!iconBefore,
			isDisabled: !!isDisabled,
			isInvalid: !!isInvalid,
			isRequired: !!isRequired,
		};

		const onChangeDebounce = useCallback(
			(ev: React.ChangeEvent<HTMLInputElement>): void => {
				if (typeof debounceMs !== 'number') return;
				if (timeout) clearTimeout(timeout);
				timeout = setTimeout(() => {
					onChange?.(ev);
				}, debounceMs);
			},
			[debounceMs, onChange]
		);

		const inputChangeHandler = useCallback(
			(ev: React.ChangeEvent<HTMLInputElement>): void => {
				if (typeof debounceMs === 'number') return onChangeDebounce(ev);
				onChange?.(ev);
			},
			[onChange, debounceMs, onChangeDebounce]
		);

		return (
			<>
				<StyledInputWrapper {...stylesProps}>
					{iconBefore ? <span className='iconBefore'>{iconBefore}</span> : null}
					<StyledInput
						{...stylesProps}
						{...rest}
						disabled={!!isDisabled}
						placeholder=' '
						onChange={onChange ? inputChangeHandler : undefined}
						ref={ref}
					/>
					<StyledInputLabel {...stylesProps}>{placeholder}</StyledInputLabel>
					{isLoading ? (
						<span className='iconAfter'>
							<Loader />
						</span>
					) : null}
					{!isLoading && !!iconAfter ? <span className='iconAfter'>{iconAfter}</span> : null}
				</StyledInputWrapper>
			</>
		);
	})
);
