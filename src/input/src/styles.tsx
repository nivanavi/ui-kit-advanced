import styled, { css } from 'styled-components';
import { InputStylesProps } from './types';
import { ThemeColorsType, THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';

export const StyledInputWrapper = styled.div<InputStylesProps & { theme: ThemeColorsType }>`
	position: relative;

	> input:focus + label,
	> input:not(:placeholder-shown) + label {
		top: 0;
		font-size: 1.2rem;
		color: ${({ theme }) => theme.input.withValue.placeholder};
		font-weight: 300;
	}

	.iconBefore,
	.iconAfter {
		position: absolute;
		top: 0;
		display: flex;
		height: 100%;
		align-items: center;
		color: ${({ theme }) => theme.input.icon.color};
		transition: all ${ANIMATION_SEC}s ease;
	}

	.iconBefore {
		left: 1.5rem;
	}

	.iconAfter {
		right: 1.5rem;
	}

	:hover {
		.iconBefore,
		.iconAfter {
			color: ${({ theme }) => theme.input.icon.hoverColor};
		}
	}

	:focus-within {
		.iconBefore,
		.iconAfter {
			color: ${({ theme }) => theme.input.icon.focusColor};
		}
	}
`;

export const StyledInput = styled.input<InputStylesProps>`
	box-sizing: border-box;
	font-size: 1.4rem;
	outline: none;
	padding: 1rem ${({ iconAfter }) => (iconAfter ? '4rem' : '1rem')} 0
		${({ iconBefore }) => (iconBefore ? '4rem' : '1rem')};
	height: 4rem;
	width: 100%;
	color: ${({ theme }) => theme.input.color};
	border: 2px solid ${({ theme }) => theme.input.border};
	background: ${({ theme }) => theme.input.bg};
	border-radius: 5px;
	transition: all ${ANIMATION_SEC}s ease;

	:hover {
		border-color: ${({ theme }) => theme.input.hover.border};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.input.hover.placeholder};
		}
	}

	:focus {
		border-color: ${({ theme }) => theme.input.focus.border};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.input.focus.placeholder} !important;
		}
	}

	:disabled {
		color: ${({ theme }) => theme.input.disabled.color};
		cursor: no-drop;
		border-color: ${({ theme }) => theme.input.disabled.border};
		background: ${({ theme }) => theme.input.disabled.bg};

		// placeholder
		& + * {
			color: ${({ theme }) => theme.input.disabled.placeholder};
		}
	}

	${({ isInvalid }) =>
		isInvalid &&
		css`
			border-color: ${({ theme }) => theme.input.invalid.border};
			background: ${({ theme }) => theme.input.invalid.bg};

			// placeholder
			& + * {
				color: ${({ theme }) => theme.input.invalid.placeholder} !important;
			}
		`}
`;

export const StyledInputLabel = styled.label<InputStylesProps>`
	position: absolute;
	white-space: nowrap;
	color: ${({ theme }) => theme.input.color};
	left: ${({ iconBefore }) => (iconBefore ? '4rem' : '1rem')};
	top: calc(50% - 1rem);
	font-size: 1.4rem;
	line-height: 2rem;
	pointer-events: none;
	transition: all ${ANIMATION_SEC}s ease;

	${({ isRequired }) =>
		isRequired &&
		css`
			::before {
				content: '* ';
				color: ${({ theme }) => theme.input.requiredColor};
			}
		`}
`;

StyledInputWrapper.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledInputLabel.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledInput.defaultProps = { theme: THEME_LIGHT_COLORS };
