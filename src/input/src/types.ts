import React from 'react';

type InputPropsGeneral = {
	isInvalid?: boolean;
	/**
	 * Состояние инпута (загрузка)
	 */
	isLoading?: boolean;
	/**
	 * Состояние инпута (заблокирован)
	 */
	isDisabled?: boolean;
	/**
	 * Иконка перед вводимым текстом
	 */
	iconBefore?: React.ReactNode;
	/**
	 * Иконка после вводимого текста
	 */
	iconAfter?: React.ReactNode;
	/**
	 * Красная звездочка для отображения обязательности поля
	 */
	isRequired?: boolean;
} & React.ComponentProps<'input'>;

type InputPropsDebounce = InputPropsGeneral & {
	/**
	 * Время на которое будет дебаунсится вызов функции onChange
	 */
	debounceMs?: number;
	value?: never;
};

type InputPropsControlled = InputPropsGeneral & {
	debounceMs?: never;
};

export type InputProps = InputPropsDebounce | InputPropsControlled;

export type InputStylesProps = {
	isDisabled: boolean;
	isInvalid: boolean;
	iconBefore: boolean;
	iconAfter: boolean;
	isRequired: boolean;
};

export type InputComponent = React.ForwardRefExoticComponent<InputProps & React.RefAttributes<HTMLInputElement>>;
