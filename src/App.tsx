import React, { useCallback, useState } from 'react';
import { ClickAway } from './hooks/src/index';

// * todo
// tests for every component
// story for every component

// * components to add
// enums
// number input
// range
// radio
// upload file

const App: React.FC = () => {
	const [state, set] = useState(false);

	const setTrue = useCallback(() => {
		set(true);
	}, []);

	const setFalse = useCallback(() => {
		console.log('set false');
		set(false);
	}, []);

	// const [state1, set1] = useState(false);
	//
	// const setTrue1 = useCallback(() => {
	// 	set1(true);
	// }, []);
	//
	// const setFalse1 = useCallback(() => {
	// 	console.log('set false 1');
	// 	set1(false);
	// }, []);

	return (
		<>
			<button type='button' onClick={setTrue}>
				show
			</button>
			<ClickAway onClickAway={setFalse}>{state ? <div>default</div> : null}</ClickAway>
			<br />
			<br />
		</>
	);
};

export default App;

// <ClickAway onClickAway={setFalse}>{state ? <div>default</div> : null}</ClickAway>

// <ClickAway onClickAway={setFalse}>
// 	<ConditionalRender isRender={state}>
// 		<div>ConditionalRender</div>
// 	</ConditionalRender>
// </ClickAway>
