type LoaderTypes = 'default' | 'portal';
export type LoaderSizes = 'm' | 'l' | 'xl';

export type LoaderProps = {
	type?: LoaderTypes;
	size?: LoaderSizes;
};
