import React from 'react';
import { LoaderIcon } from '@ui-kit-advanced/icons';
import {
	StyledBluePortal,
	StyledBox,
	StyledHill,
	StyledLoader,
	StyledOrangePortal,
	StyledPortalLoader,
} from './styles';
import { LoaderProps } from './types';

export const Loader: React.FC<LoaderProps> = React.memo(props => {
	const { size = 'm', type = 'default' } = props;

	switch (type) {
		case 'default':
			return (
				<StyledLoader size={size}>
					<LoaderIcon />
				</StyledLoader>
			);
		case 'portal':
			return (
				<StyledPortalLoader size={size}>
					<StyledBox />
					<StyledBluePortal />
					<StyledOrangePortal />
					<StyledHill />
				</StyledPortalLoader>
			);
		default:
			return null;
	}
});
