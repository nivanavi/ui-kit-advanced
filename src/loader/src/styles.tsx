import styled from 'styled-components';
import { LoaderSizes } from './types';

const getWidthForDefault = (size: LoaderSizes) => {
	switch (size) {
		case 'm':
			return 2;
		case 'l':
			return 4;
		case 'xl':
			return 6;
		default:
			return 2;
	}
};

const getSizeForPortal = (size: LoaderSizes) => {};

// default loader
export const StyledLoader = styled.div<{ size: LoaderSizes }>`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 100%;

	p {
		opacity: 0.4;
		margin-top: 1rem;
	}

	> svg {
		${({ size }) => {
			const width = getWidthForDefault(size);
			return `
      min-width: ${width}rem;
      max-width: ${width}rem;
      min-height: ${width}rem;
      max-height: ${width}rem;
      `;
		}}

		animation: rotate 1s infinite linear;
	}

	@keyframes rotate {
		from {
			transform: rotate(0deg);
		}

		to {
			transform: rotate(360deg);
		}
	}
`;

// portal loader
const animationTime = 3;
const sideWidth = 10;
const lineWidth = 3;

export const StyledPortalLoader = styled.div<{ size?: LoaderSizes }>`
	position: relative;
	width: ${sideWidth}rem;
	height: ${sideWidth}rem;

	@keyframes portal {
		0% {
			opacity: 0;
		}
		80% {
			opacity: 0;
			width: 0;
		}
		90% {
			opacity: 1;
			width: ${(sideWidth * 40) / 100}rem;
		}
		100% {
			opacity: 0;
		}
	}
`;

export const StyledHill = styled.div`
	position: absolute;
	left: -${(sideWidth * 20) / 100}rem;
	bottom: ${(sideWidth * 50) / 100}rem;
	width: ${Math.floor(Math.sqrt(2) * sideWidth)}rem;
	height: ${lineWidth}px;
	background: purple;
	transform: rotate(-45deg);
`;

export const StyledBluePortal = styled.div`
	display: flex;
	justify-content: center;

	::after {
		content: '';
		display: block;
		width: 0;
		height: ${lineWidth}px;
		background: #16bbe3;
		opacity: 0;
		transform: translate(${(sideWidth * 40) / 100}rem, ${sideWidth}rem);
		animation: portal ${animationTime}s cubic-bezier(0.79, 0, 0.47, 0.97) infinite;
	}
`;

export const StyledOrangePortal = styled.div`
	display: flex;
	justify-content: center;

	::after {
		content: '';
		display: block;
		width: 0;
		height: ${lineWidth}px;
		background: #ef8e1f;
		opacity: 0;
		transform: translate(-${(sideWidth * 40) / 100}rem, 0);
		animation: portal ${animationTime}s cubic-bezier(0.79, 0, 0.47, 0.97) infinite;
		animation-delay: 0.1s;
	}
`;

export const StyledBox = styled.div`
	position: absolute;
	width: ${(sideWidth * 15) / 100}rem;
	height: ${(sideWidth * 15) / 100}rem;
	border: ${lineWidth}px solid purple;
	transform: translate(0, 5rem) rotate(-45deg);
	animation: tryToTop ${animationTime}s cubic-bezier(0.79, 0, 0.47, 0.97) infinite;

	@keyframes tryToTop {
		0% {
			transform: translate(0, ${(sideWidth * 50) / 100}rem) rotate(0deg);
			//transform: translate(-4rem, 3.5rem) rotate(0deg);
			border-radius: 0 0 40% 0;
		}
		10% {
			transform: translate(${(sideWidth * 10) / 100}rem, ${(sideWidth * 40) / 100}rem) rotate(90deg);
			//transform: translate(-3rem, 2.5rem) rotate(90deg);
			border-radius: 0 40% 0 0;
		}
		20% {
			transform: translate(${(sideWidth * 20) / 100}rem, ${(sideWidth * 30) / 100}rem) rotate(180deg);
			border-radius: 40% 0 0 0;
		}
		30% {
			transform: translate(${(sideWidth * 30) / 100}rem, ${(sideWidth * 20) / 100}rem) rotate(270deg);
			border-radius: 0 0 0 40%;
		}
		40% {
			transform: translate(${(sideWidth * 40) / 100}rem, ${(sideWidth * 10) / 100}rem) rotate(360deg);
			border-radius: 0 0 40% 0;
		}
		50% {
			transform: translate(${(sideWidth * 50) / 100}rem, 0) rotate(450deg);
			border-radius: 0 40% 0 0;
		}
		60% {
			transform: translate(${(sideWidth * 60) / 100}rem, -${(sideWidth * 10) / 100}rem) rotate(540deg);
			border-radius: 40% 0 0 0;
		}
		70% {
			transform: translate(${(sideWidth * 70) / 100}rem, -${(sideWidth * 20) / 100}rem) rotate(630deg);
			border-radius: 0 0 0 40%;
		}
		80% {
			transform: translate(${(sideWidth * 80) / 100}rem, -${(sideWidth * 30) / 100}rem) rotate(720deg);
			//width: 1.5rem;
			//height: 1.5rem;
			border-radius: 0 0 0 0;
			opacity: 1;
		}
		85% {
			opacity: 1;
		}
		90% {
			transform: translate(${(sideWidth * 80) / 100}rem, ${(sideWidth * 80) / 100}rem) rotate(720deg);
			opacity: 0;
		}
		91% {
			transform: translate(0, ${(sideWidth * 5) / 100}rem) rotate(0deg);
			opacity: 0;
		}
		95% {
			opacity: 1;
		}
		100% {
			transform: translate(0, ${(sideWidth * 50) / 100}rem) rotate(0deg);
			border-radius: 0 0 0 0;
			opacity: 1;
		}
	}
`;
