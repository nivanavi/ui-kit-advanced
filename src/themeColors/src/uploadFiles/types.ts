type ConditionColorsType = {
	border: string;
	bg: string;
	color: string;
	placeholder: string;
};

export type UploadFilesThemeColors = {
	requiredColor: string;
	hover: ConditionColorsType;
	disabled: ConditionColorsType;
	invalid: ConditionColorsType;
} & ConditionColorsType;
