import { UploadFilesThemeColors } from './types';
import { basicColors, dark, light } from '../colors';

export const uploadFilesLightColors: UploadFilesThemeColors = {
	color: light.grey9,
	border: light.grey3,
	bg: basicColors.transparent,
	placeholder: light.grey4,
	requiredColor: light.red5,
	hover: {
		color: light.grey9,
		border: light.blue3,
		bg: light.blue0,
		placeholder: light.blue3,
	},
	disabled: {
		color: light.grey5,
		border: light.grey2,
		bg: light.grey0,
		placeholder: light.grey4,
	},
	invalid: {
		color: light.grey9,
		border: light.red5,
		bg: light.red0,
		placeholder: light.red3,
	},
};

export const uploadFilesDarkColors: UploadFilesThemeColors = {
	color: dark.grey1,
	border: dark.grey6,
	bg: basicColors.transparent,
	placeholder: dark.grey4,
	requiredColor: dark.red5,
	hover: {
		color: dark.grey1,
		border: dark.blue4,
		bg: dark.grey6,
		placeholder: dark.blue3,
	},
	disabled: {
		color: dark.grey6,
		border: dark.grey7,
		bg: dark.grey8,
		placeholder: dark.grey6,
	},
	invalid: {
		color: dark.grey1,
		border: dark.red4,
		bg: basicColors.transparent,
		placeholder: dark.red3,
	},
};
