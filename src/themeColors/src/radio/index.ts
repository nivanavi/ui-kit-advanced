import { RadioThemeColors } from './types';
import { basicColors, dark, light } from '../colors';

// color: light.grey8,
// hover: {
// 	color: light.grey5,
// },
// active: {
// 	color: light.grey9,
// },
// disabled: {
// 	color: light.grey5,
// },
// invalid: {
// 	color: light.red5,
// },
//
// circle: {
// 	border: light.blue5,
// 	checked: {
// 		bg: light.blue5,
// 	},
// 	invalid: {
// 		border: light.red5,
// 	},
// },

export const radioLightColors: RadioThemeColors = {
	color: light.grey8,
	hoverColor: light.grey5,
	activeColor: light.grey9,
	disabledColor: light.grey5,
	invalidColor: light.red5,
	circle: {
		bg: basicColors.transparent,
		border: light.blue5,
		checked: {
			bg: light.blue5,
			border: light.blue5,
		},
		invalid: {
			bg: basicColors.transparent,
			border: light.red5,
		},
	},
};

export const radioDarkColors: RadioThemeColors = {
	color: dark.grey1,
	hoverColor: dark.grey0,
	activeColor: dark.grey2,
	disabledColor: dark.grey5,
	invalidColor: dark.red5,
	circle: {
		bg: basicColors.transparent,
		border: dark.blue5,
		checked: {
			bg: dark.blue5,
			border: dark.blue5,
		},
		invalid: {
			bg: basicColors.transparent,
			border: dark.red5,
		},
	},
};
