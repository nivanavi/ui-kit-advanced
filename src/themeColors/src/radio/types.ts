type ConditionColorsType = {
	border: string;
	bg: string;
};

export type RadioThemeColors = {
	color: string;
	hoverColor: string;
	activeColor: string;
	disabledColor: string;
	invalidColor: string;
	circle: {
		checked: ConditionColorsType;
		invalid: ConditionColorsType;
	} & ConditionColorsType;
};
