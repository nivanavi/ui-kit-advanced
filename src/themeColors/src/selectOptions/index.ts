import { SelectOptionsThemeColors } from './types';
import { basicColors, dark, light } from '../colors';

export const selectOptionsLightColors: SelectOptionsThemeColors = {
	bg: light.grey0,
	border: light.blue4,
	dropDownHelpMessage: {
		bg: basicColors.white,
	},
	highlightColor: basicColors.blue5_20,
	item: {
		color: light.grey4,
		selectedColor: light.grey9,
		hoverColor: light.grey6,
		hoverBg: light.grey2,
	},
};

export const selectOptionsDarkColors: SelectOptionsThemeColors = {
	bg: dark.grey8,
	border: dark.blue4,
	dropDownHelpMessage: {
		bg: dark.grey4,
	},
	highlightColor: basicColors.blue5_20,
	item: {
		color: dark.grey1,
		selectedColor: dark.grey0,
		hoverColor: dark.grey0,
		hoverBg: dark.grey7,
	},
};
