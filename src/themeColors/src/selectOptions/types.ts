export type SelectOptionsThemeColors = {
	bg: string;
	border: string;
	dropDownHelpMessage: {
		bg: string;
	};
	highlightColor: string;
	item: {
		color: string;
		selectedColor: string;
		hoverColor: string;
		hoverBg: string;
	};
};
