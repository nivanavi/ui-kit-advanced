type ConditionColorsType = {
	color: string;
	border: string;
	bg: string;
	placeholder: string;
};

export type InputThemeColors = {
	color: string;
	border: string;
	bg: string;
	placeholder: string;
	requiredColor: string;
	hover: ConditionColorsType;
	focus: ConditionColorsType;
	invalid: ConditionColorsType;
	disabled: ConditionColorsType;
	withValue: ConditionColorsType;
	icon: {
		color: string;
		hoverColor: string;
		focusColor: string;
	};
};
