import { InputThemeColors } from './types';
import { dark, light, basicColors } from '../colors';

export const inputLightColors: InputThemeColors = {
	color: light.grey9,
	border: light.grey3,
	bg: basicColors.transparent,
	placeholder: light.grey4,
	requiredColor: light.red5,
	hover: {
		color: light.grey9,
		bg: basicColors.transparent,
		border: light.blue3,
		placeholder: light.blue3,
	},
	focus: {
		color: light.grey9,
		bg: basicColors.transparent,
		border: light.blue4,
		placeholder: light.blue4,
	},
	invalid: {
		color: light.grey9,
		bg: light.red0,
		border: light.red4,
		placeholder: light.red3,
	},
	disabled: {
		color: light.grey5,
		bg: light.grey0,
		border: light.grey2,
		placeholder: light.grey4,
	},
	withValue: {
		color: light.grey9,
		border: light.grey3,
		bg: basicColors.transparent,
		placeholder: light.grey3,
	},
	icon: {
		color: light.grey7,
		hoverColor: light.blue3,
		focusColor: light.blue4,
	},
};

export const inputDarkColors: InputThemeColors = {
	color: dark.grey0,
	bg: dark.grey9,
	border: dark.grey5,
	placeholder: dark.grey3,
	requiredColor: dark.red5,
	hover: {
		color: dark.grey0,
		bg: dark.grey9,
		border: dark.blue3,
		placeholder: dark.blue2,
	},
	focus: {
		color: dark.grey0,
		bg: dark.grey9,
		border: dark.blue4,
		placeholder: dark.blue3,
	},
	invalid: {
		color: dark.grey0,
		bg: basicColors.transparent,
		border: dark.red5,
		placeholder: dark.red2,
	},
	disabled: {
		color: dark.grey5,
		bg: dark.grey9,
		border: dark.grey8,
		placeholder: dark.grey5,
	},
	withValue: {
		color: dark.grey3,
		bg: dark.grey5,
		border: dark.grey9,
		placeholder: dark.grey2,
	},
	icon: {
		color: dark.grey6,
		hoverColor: dark.blue2,
		focusColor: dark.blue3,
	},
};
