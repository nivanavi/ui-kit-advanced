import { ThemeColorsType } from './types';
import { inputDarkColors, inputLightColors } from './input';
import { buttonDarkColors, buttonLightColors } from './button';
import { accordionDarkColors, accordionLightColors } from './accordion';
import { checkboxDarkColors, checkboxLightColors } from './checkbox';
import { textareaDarkColors, textareaLightColors } from './textarea';
import { radioDarkColors, radioLightColors } from './radio';
import { uploadFilesDarkColors, uploadFilesLightColors } from './uploadFiles';
import { messageDarkColors, messageLightColors } from './message';
import { selectOptionsDarkColors, selectOptionsLightColors } from './selectOptions';

export const THEME_LIGHT_COLORS: ThemeColorsType = {
	input: inputLightColors,
	button: buttonLightColors,
	accordion: accordionLightColors,
	checkbox: checkboxLightColors,
	textarea: textareaLightColors,
	radio: radioLightColors,
	uploadFiles: uploadFilesLightColors,
	message: messageLightColors,
	selectOptions: selectOptionsLightColors,
};

export const THEME_DARK_COLORS: ThemeColorsType = {
	input: inputDarkColors,
	button: buttonDarkColors,
	accordion: accordionDarkColors,
	checkbox: checkboxDarkColors,
	textarea: textareaDarkColors,
	radio: radioDarkColors,
	uploadFiles: uploadFilesDarkColors,
	message: messageDarkColors,
	selectOptions: selectOptionsDarkColors,
};

// export const lightTheme = {
// 	// general
// 	text: {
// 		primary: '#24292e',
// 		secondary: '#586069',
// 		placeholder: '#6a737d',
// 		disabled: '#959da5',
// 		link: '#0366d6',
// 		danger: '#cb2431',
// 		success: '#22863a',
// 		warning: '#b08800',
// 		info: '#1074e7',
// 	},
//
// 	bg: {
// 		primary: '#ffffff',
// 		secondary: '#fafbfc',
// 		danger: '#ffeef0',
// 		success: '#dcffe4',
// 		warning: '#fff5b1',
// 		info: '#f1f8ff',
// 	},
//
// 	// components
//
// 	tooltip: {
// 		bg: light.grey9,
// 		color: light.grey0,
// 	},
//
// 	daData: {
// 		bg: light.grey0,
// 		border: light.blue4,
// 		hover: {
// 			bg: light.grey2,
// 		},
// 		highlighted: light.blue6,
// 	},
//
// 	// datePicker: {},
//
// 	dropdownMenu: {
// 		bg: '#ffffff',
// 		shadow: 'rgb(9 30 66 / 25%) 0px 4px 8px -2px, rgb(9 30 66 / 31%) 0px 0px 1px;',
// 		item: {
// 			color: light.grey8,
// 			hover: {
// 				bg: light.grey0,
// 				color: light.grey7,
// 			},
// 			active: {
// 				bg: light.grey1,
// 				color: light.grey9,
// 			},
// 		},
// 	},
//
// 	modal: {
// 		backdrop: 'rgba(0,0,0,0.4)',
// 		bg: '#ffffff',
// 		color: light.grey9,
// 		icon: {
// 			color: light.grey6,
// 			hover: {
// 				color: light.grey5,
// 			},
// 			active: {
// 				color: light.grey7,
// 			},
// 		},
// 	},
//
// 	// range: {},
//
// 	pagination: {
// 		btn: {
// 			color: light.blue5,
// 			hover: {
// 				bg: light.blue1,
// 			},
// 			current: {
// 				bg: light.blue5,
// 				color: '#ffffff',
// 				hover: {
// 					bg: light.blue4,
// 				},
// 			},
// 		},
// 		arrow: {
// 			color: light.blue5,
// 		},
// 	},
// 	table: {
// 		header: {
// 			color: light.grey0,
// 			bg: light.blue5,
// 		},
// 		row: {
// 			border: light.blue5,
// 			oddBg: light.blue0,
// 			hover: {
// 				bg: light.blue5_20,
// 			},
// 		},
// 	},
//
// 	tabs: {
// 		item: {
// 			hover: {
// 				bg: light.grey0,
// 			},
// 		},
// 		selected: {
// 			border: light.blue5,
// 		},
// 	},
//
// 	toggleCheckbox: {
// 		border: light.green4,
// 		checked: {
// 			bg: light.green4,
// 		},
// 		circle: {
// 			border: light.green4,
// 			bg: '#ffffff',
// 		},
// 	},
// };
//
// export const darkTheme = {
// 	text: {
// 		primary: '#adbac7',
// 		secondary: '#768390',
// 		placeholder: '#545d68',
// 		disabled: '#545d68',
// 		link: '#539bf5',
// 		danger: '#e5534b',
// 		success: '#6bc46d',
// 		warning: '#daaa3f',
// 		info: '#3877d5',
// 	},
//
// 	bg: {
// 		primary: '#22272e',
// 		secondary: '#22272e',
// 		danger: 'rgba(229,83,75,0.1)',
// 		success: 'rgba(70,149,74,0.1)',
// 		warning: 'rgba(174,124,20,0.1)',
// 		info: 'rgba(65,132,228,0.1)',
// 	},
//
// 	// components
// 	tooltip: {
// 		bg: dark.grey0,
// 		color: dark.grey9,
// 	},
//
// 	daData: {
// 		bg: dark.grey8,
// 		border: dark.blue5,
// 		hover: {
// 			bg: dark.grey7,
// 		},
// 		highlighted: dark.blue5,
// 	},
//
// 	// datePicker: {},
//
// 	dropdownMenu: {
// 		bg: dark.grey8,
// 		shadow: 'rgb(9 30 66 / 25%) 0px 4px 8px -2px, rgb(9 30 66 / 31%) 0px 0px 1px;',
// 		item: {
// 			color: dark.grey1,
// 			hover: {
// 				bg: dark.grey7,
// 				color: dark.grey0,
// 			},
// 			active: {
// 				bg: dark.grey9,
// 				color: dark.grey0,
// 			},
// 		},
// 	},
//
// 	toggleCheckbox: {
// 		border: dark.green2,
// 		checked: {
// 			bg: dark.green2,
// 		},
// 		circle: {
// 			border: dark.green2,
// 			bg: '#ffffff',
// 		},
// 	},
//
// 	modal: {
// 		backdrop: 'rgba(255,255,255,0.4)',
// 		bg: dark.grey9,
// 		color: dark.grey0,
// 		icon: {
// 			color: dark.grey4,
// 			hover: {
// 				color: dark.grey2,
// 			},
// 			active: {
// 				color: dark.grey6,
// 			},
// 		},
// 	},
//
// 	// range: {},
// 	pagination: {
// 		btn: {
// 			color: dark.blue5,
// 			hover: {
// 				bg: dark.grey8,
// 			},
// 			current: {
// 				bg: dark.blue5,
// 				color: '#ffffff',
// 				hover: {
// 					bg: dark.blue4,
// 				},
// 			},
// 		},
// 		arrow: {
// 			color: dark.blue5,
// 		},
// 	},
//
// 	table: {
// 		header: {
// 			color: dark.grey0,
// 			bg: dark.blue5,
// 		},
// 		row: {
// 			border: dark.grey4,
// 			bg: dark.grey8,
// 			oddBg: dark.grey7,
// 			hover: {
// 				bg: dark.blue5_20,
// 			},
// 		},
// 	},
//
// 	tabs: {
// 		item: {
// 			hover: {
// 				bg: dark.grey8,
// 			},
// 		},
// 		selected: {
// 			border: dark.blue5,
// 		},
// 	},
// };
