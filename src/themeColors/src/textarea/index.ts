import { TextareaThemeColors } from './types';
import { light, dark, basicColors } from '../colors';

export const textareaLightColors: TextareaThemeColors = {
	color: light.grey9,
	border: light.grey3,
	placeholder: light.grey4,
	bg: basicColors.transparent,
	requiredColor: light.red5,
	hover: {
		color: light.grey9,
		border: light.blue3,
		placeholder: light.grey4,
		bg: basicColors.transparent,
	},
	focus: {
		color: light.grey9,
		border: light.blue4,
		placeholder: light.grey4,
		bg: basicColors.transparent,
	},
	invalid: {
		color: light.grey9,
		border: light.red5,
		placeholder: light.grey4,
		bg: light.red0,
	},
	disabled: {
		color: light.grey5,
		border: light.grey2,
		placeholder: light.grey4,
		bg: light.red0,
	},
	withValue: {
		color: light.grey9,
		border: light.grey3,
		placeholder: light.grey3,
		bg: basicColors.transparent,
	},
	icon: {
		color: light.grey7,
		hoverColor: light.blue3,
		focusColor: light.blue4,
	},
};

export const textareaDarkColors: TextareaThemeColors = {
	color: dark.grey0,
	border: dark.grey5,
	placeholder: dark.grey3,
	bg: dark.grey9,
	requiredColor: dark.red5,
	hover: {
		color: dark.grey0,
		border: dark.blue3,
		placeholder: dark.blue2,
		bg: dark.grey9,
	},
	focus: {
		color: dark.grey0,
		border: dark.blue4,
		placeholder: dark.blue3,
		bg: dark.grey9,
	},
	invalid: {
		color: dark.grey0,
		border: dark.red5,
		placeholder: dark.red2,
		bg: basicColors.transparent,
	},
	disabled: {
		color: dark.grey5,
		border: dark.grey8,
		placeholder: dark.grey5,
		bg: dark.grey9,
	},
	withValue: {
		color: dark.grey0,
		border: dark.grey5,
		placeholder: dark.grey2,
		bg: dark.grey9,
	},
	icon: {
		color: dark.grey6,
		hoverColor: dark.blue2,
		focusColor: dark.blue3,
	},
};
