type ConditionColorsType = {
	color: string;
	border: string;
	bg: string;
	placeholder: string;
};

export type TextareaThemeColors = {
	color: string;
	border: string;
	requiredColor: string;
	placeholder: string;
	bg: string;
	hover: ConditionColorsType;
	focus: ConditionColorsType;
	invalid: ConditionColorsType;
	disabled: ConditionColorsType;
	withValue: ConditionColorsType;
	icon: {
		color: string;
		hoverColor: string;
		focusColor: string;
	};
};
