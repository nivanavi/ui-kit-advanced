import { ButtonThemeColors } from './types';
import { dark, light, basicColors } from '../colors';

export const buttonLightColors: ButtonThemeColors = {
	common: {
		// from success
		filled: {
			color: basicColors.white,
			bg: light.green5,
			border: light.green5,
			disabled: {
				bg: light.grey5,
				border: light.grey5,
				color: light.grey3,
			},
			hover: {
				bg: light.green4,
				border: light.green4,
			},
			active: {
				bg: light.green6,
				border: light.green6,
			},
		},
		transparent: {
			color: light.green6,
			bg: basicColors.transparent,
			border: light.green6,
			hover: {
				bg: light.green6,
				color: basicColors.white,
			},
			active: {
				bg: light.green7,
				border: light.green8,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
		},
		link: {
			color: light.green6,
			hover: {
				color: light.green4,
			},
			active: {
				color: light.green8,
			},
			disabled: {
				color: light.grey5,
			},
		},
		text: {
			color: light.green3,
			hover: {
				color: light.green6,
			},
			active: {
				color: light.green8,
			},
			disabled: {
				color: light.grey5,
			},
		},
		icon: {
			color: light.green3,
			hover: {
				color: light.green6,
			},
			active: {
				color: light.green8,
			},
			disabled: {
				color: light.grey5,
			},
		},
		// diff
		flag: {
			color: light.grey2,
			bg: basicColors.transparent,
			hover: {
				bg: light.grey8,
				color: light.grey0,
			},
			active: {
				bg: light.grey7,
				color: light.grey1,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey8,
			},
			selected: {
				bg: light.grey8,
				color: light.pink5,
			},
		},
	},
	default: {
		filled: {
			color: light.grey9,
			bg: light.grey0,
			border: light.grey1,
			disabled: {
				bg: light.grey5,
				border: light.grey6,
				color: light.grey3,
			},
			hover: {
				bg: light.grey1,
				border: light.grey2,
			},
			active: {
				bg: light.grey2,
				border: light.grey3,
			},
		},
		transparent: {
			color: light.grey7,
			bg: basicColors.transparent,
			border: light.grey7,
			hover: {
				bg: light.grey7,
				color: basicColors.white,
			},
			active: {
				bg: light.grey8,
				border: light.grey8,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
		},
		link: {
			color: light.grey8,
			hover: {
				color: light.grey7,
			},
			active: {
				color: light.grey9,
			},
			disabled: {
				color: light.grey5,
			},
		},
		text: {
			color: light.grey8,
			hover: {
				color: light.grey7,
			},
			active: {
				color: light.grey9,
			},
			disabled: {
				color: light.grey5,
			},
		},
		icon: {
			color: light.grey8,
			hover: {
				color: light.grey7,
			},
			active: {
				color: light.grey9,
			},
			disabled: {
				color: light.grey5,
			},
		},
		flag: {
			color: light.grey6,
			bg: basicColors.transparent,
			hover: {
				bg: light.grey1,
				color: light.grey7,
			},
			active: {
				bg: light.grey2,
				color: light.grey8,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey1,
			},
			selected: {
				bg: basicColors.transparent,
				color: light.grey9,
			},
		},
	},
	success: {
		filled: {
			color: basicColors.white,
			bg: light.green5,
			border: light.green5,
			disabled: {
				bg: light.grey5,
				border: light.grey5,
				color: light.grey3,
			},
			hover: {
				bg: light.green4,
				border: light.green4,
			},
			active: {
				bg: light.green6,
				border: light.green6,
			},
		},
		transparent: {
			color: light.green6,
			bg: basicColors.transparent,
			border: light.green6,
			hover: {
				bg: light.green6,
				color: basicColors.white,
			},
			active: {
				bg: light.green7,
				border: light.green8,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
		},
		link: {
			color: light.green5,
			hover: {
				color: light.green3,
			},
			active: {
				color: light.green7,
			},
			disabled: {
				color: light.grey5,
			},
		},
		text: {
			color: light.green5,
			hover: {
				color: light.green3,
			},
			active: {
				color: light.green7,
			},
			disabled: {
				color: light.grey5,
			},
		},
		icon: {
			color: light.green5,
			hover: {
				color: light.green3,
			},
			active: {
				color: light.green7,
			},
			disabled: {
				color: light.grey5,
			},
		},
		flag: {
			color: light.grey6,
			bg: basicColors.transparent,
			hover: {
				bg: light.green0,
				color: light.green4,
			},
			active: {
				bg: light.green1,
				color: light.green7,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey1,
			},
			selected: {
				bg: basicColors.transparent,
				color: light.green5,
			},
		},
	},
	info: {
		filled: {
			color: basicColors.white,
			bg: light.blue5,
			border: light.blue5,
			disabled: {
				bg: light.grey5,
				border: light.grey5,
				color: light.grey3,
			},
			hover: {
				bg: light.blue4,
				border: light.blue4,
			},
			active: {
				bg: light.blue6,
				border: light.blue6,
			},
		},
		transparent: {
			color: light.blue6,
			bg: basicColors.transparent,
			border: light.blue6,
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
			hover: {
				bg: light.blue6,
				color: basicColors.white,
			},
			active: {
				bg: light.blue7,
				border: light.blue8,
				color: basicColors.white,
			},
		},
		link: {
			color: light.blue5,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.blue3,
			},
			active: {
				color: light.blue7,
			},
		},
		text: {
			color: light.blue5,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.blue3,
			},
			active: {
				color: light.blue7,
			},
		},
		icon: {
			color: light.blue5,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.blue3,
			},
			active: {
				color: light.blue7,
			},
		},
		flag: {
			color: light.grey6,
			bg: basicColors.transparent,
			hover: {
				bg: light.blue0,
				color: light.blue4,
			},
			active: {
				bg: light.blue1,
				color: light.blue7,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey1,
			},
			selected: {
				bg: basicColors.transparent,
				color: light.blue5,
			},
		},
	},
	warning: {
		filled: {
			color: basicColors.white,
			bg: light.yellow5,
			border: light.yellow5,
			disabled: {
				bg: light.grey5,
				border: light.grey5,
				color: light.grey3,
			},
			hover: {
				bg: light.yellow4,
				border: light.yellow4,
			},
			active: {
				bg: light.yellow6,
				border: light.yellow6,
			},
		},
		transparent: {
			color: light.yellow6,
			bg: basicColors.transparent,
			border: light.yellow6,
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
			hover: {
				bg: light.yellow6,
				color: basicColors.white,
			},
			active: {
				bg: light.yellow7,
				border: light.yellow8,
				color: basicColors.white,
			},
		},
		link: {
			color: light.yellow6,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.yellow4,
			},
			active: {
				color: light.yellow8,
			},
		},
		text: {
			color: light.yellow6,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.yellow4,
			},
			active: {
				color: light.yellow8,
			},
		},
		icon: {
			color: light.yellow6,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.yellow4,
			},
			active: {
				color: light.yellow8,
			},
		},
		flag: {
			color: light.grey6,
			bg: basicColors.transparent,
			hover: {
				bg: light.yellow0,
				color: light.yellow4,
			},
			active: {
				bg: light.yellow1,
				color: light.yellow7,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey1,
			},
			selected: {
				bg: basicColors.transparent,
				color: light.yellow5,
			},
		},
	},
	danger: {
		filled: {
			color: basicColors.white,
			bg: light.red5,
			border: light.red5,
			disabled: {
				bg: light.grey5,
				border: light.grey5,
				color: light.grey3,
			},
			hover: {
				bg: light.red4,
				border: light.red4,
			},
			active: {
				bg: light.red6,
				border: light.red6,
			},
		},
		transparent: {
			color: light.red6,
			bg: basicColors.transparent,
			border: light.red6,
			disabled: {
				bg: basicColors.transparent,
				color: light.grey5,
				border: light.grey5,
			},
			hover: {
				bg: light.red6,
				color: basicColors.white,
			},
			active: {
				bg: light.red7,
				border: light.red8,
				color: basicColors.white,
			},
		},
		link: {
			color: light.red6,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.red4,
			},
			active: {
				color: light.red8,
			},
		},
		text: {
			color: light.red3,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.red6,
			},
			active: {
				color: light.red8,
			},
		},
		icon: {
			color: light.red5,
			disabled: {
				color: light.grey5,
			},
			hover: {
				color: light.red3,
			},
			active: {
				color: light.red7,
			},
		},
		flag: {
			color: light.grey6,
			bg: basicColors.transparent,
			hover: {
				bg: light.red0,
				color: light.red4,
			},
			active: {
				bg: light.red1,
				color: light.red7,
			},
			disabled: {
				color: light.grey5,
				bg: light.grey1,
			},
			selected: {
				bg: basicColors.transparent,
				color: light.red5,
			},
		},
	},
};

export const buttonDarkColors: ButtonThemeColors = {
	common: {
		// from success
		filled: {
			color: basicColors.white,
			bg: dark.green5,
			border: dark.green5,
			disabled: {
				bg: dark.grey5,
				border: dark.grey5,
				color: dark.grey3,
			},
			hover: {
				bg: dark.green4,
				border: dark.green4,
			},
			active: {
				bg: dark.green6,
				border: dark.green6,
			},
		},
		transparent: {
			color: dark.green6,
			bg: basicColors.transparent,
			border: dark.green6,
			hover: {
				bg: dark.green6,
				color: basicColors.white,
			},
			active: {
				bg: dark.green7,
				border: dark.green8,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey5,
				border: dark.grey5,
			},
		},
		link: {
			color: dark.green6,
			hover: {
				color: dark.green4,
			},
			active: {
				color: dark.green8,
			},
			disabled: {
				color: dark.grey5,
			},
		},
		text: {
			color: dark.green3,
			hover: {
				color: dark.green6,
			},
			active: {
				color: dark.green8,
			},
			disabled: {
				color: dark.grey5,
			},
		},
		icon: {
			color: dark.green3,
			hover: {
				color: dark.green6,
			},
			active: {
				color: dark.green8,
			},
			disabled: {
				color: dark.grey5,
			},
		},
		// diff
		flag: {
			color: dark.grey2,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.grey0,
			},
			active: {
				bg: dark.grey7,
				color: dark.grey1,
			},
			disabled: {
				color: dark.grey5,
				bg: dark.grey7,
			},
			selected: {
				bg: dark.grey9,
				color: dark.pink4,
			},
		},
	},
	default: {
		filled: {
			color: dark.grey0,
			bg: dark.grey5,
			border: dark.grey5,
			disabled: {
				bg: dark.grey7,
				border: dark.grey7,
				color: dark.grey3,
			},
			hover: {
				bg: dark.grey4,
				border: dark.grey4,
			},
			active: {
				bg: dark.grey6,
				border: dark.grey6,
			},
		},
		transparent: {
			color: dark.grey5,
			bg: basicColors.transparent,
			border: dark.grey5,
			hover: {
				bg: dark.grey5,
				color: basicColors.white,
			},
			active: {
				bg: dark.grey7,
				border: dark.grey7,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey7,
				border: dark.grey7,
			},
		},
		link: {
			color: dark.grey3,
			hover: {
				color: dark.grey1,
			},
			active: {
				color: dark.grey5,
			},
			disabled: {
				color: dark.grey7,
			},
		},
		text: {
			color: dark.grey3,
			hover: {
				color: dark.grey1,
			},
			active: {
				color: dark.grey5,
			},
			disabled: {
				color: dark.grey7,
			},
		},
		icon: {
			color: dark.grey5,
			hover: {
				color: dark.grey3,
			},
			active: {
				color: dark.grey7,
			},
			disabled: {
				color: dark.grey8,
			},
		},
		flag: {
			color: dark.grey5,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.grey3,
			},
			active: {
				bg: dark.grey9,
				color: dark.grey7,
			},
			disabled: {
				color: dark.grey8,
				bg: dark.grey6,
			},
			selected: {
				bg: dark.grey8,
				color: dark.grey0,
			},
		},
	},
	success: {
		filled: {
			color: basicColors.white,
			bg: dark.green5,
			border: dark.green5,
			disabled: {
				bg: dark.grey7,
				border: dark.grey7,
				color: dark.grey3,
			},
			hover: {
				bg: dark.green4,
				border: dark.green4,
			},
			active: {
				bg: dark.green6,
				border: dark.green6,
			},
		},
		transparent: {
			color: dark.green4,
			bg: basicColors.transparent,
			border: dark.green4,
			hover: {
				bg: dark.green4,
				color: basicColors.white,
			},
			active: {
				bg: dark.green6,
				border: dark.green7,
				color: basicColors.white,
			},
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey7,
				border: dark.grey7,
			},
		},
		link: {
			color: dark.green4,
			hover: {
				color: dark.green2,
			},
			active: {
				color: dark.green6,
			},
			disabled: {
				color: dark.grey7,
			},
		},
		text: {
			color: dark.green4,
			hover: {
				color: dark.green2,
			},
			active: {
				color: dark.green6,
			},
			disabled: {
				color: dark.grey7,
			},
		},
		icon: {
			color: dark.green5,
			hover: {
				color: dark.green3,
			},
			active: {
				color: dark.green7,
			},
			disabled: {
				color: dark.grey8,
			},
		},
		flag: {
			color: dark.grey5,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.green3,
			},
			active: {
				bg: dark.grey9,
				color: dark.green7,
			},
			disabled: {
				color: dark.grey8,
				bg: dark.grey6,
			},
			selected: {
				bg: dark.grey8,
				color: dark.green5,
			},
		},
	},
	info: {
		filled: {
			color: basicColors.white,
			bg: dark.blue5,
			border: dark.blue5,
			disabled: {
				bg: dark.grey7,
				border: dark.grey7,
				color: dark.grey3,
			},
			hover: {
				bg: dark.blue4,
				border: dark.blue4,
			},
			active: {
				bg: dark.blue6,
				border: dark.blue6,
			},
		},
		transparent: {
			color: dark.blue6,
			bg: basicColors.transparent,
			border: dark.blue6,
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey7,
				border: dark.grey7,
			},
			hover: {
				bg: dark.blue6,
				color: basicColors.white,
			},
			active: {
				bg: dark.blue7,
				border: dark.blue8,
				color: basicColors.white,
			},
		},
		link: {
			color: dark.blue5,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.blue3,
			},
			active: {
				color: dark.blue7,
			},
		},
		text: {
			color: dark.blue5,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.blue3,
			},
			active: {
				color: dark.blue7,
			},
		},
		icon: {
			color: dark.blue5,
			disabled: {
				color: dark.grey8,
			},
			hover: {
				color: dark.blue3,
			},
			active: {
				color: dark.blue7,
			},
		},
		flag: {
			color: dark.grey5,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.blue3,
			},
			active: {
				bg: dark.grey9,
				color: dark.blue7,
			},
			disabled: {
				color: dark.grey8,
				bg: dark.grey6,
			},
			selected: {
				bg: dark.grey8,
				color: dark.blue5,
			},
		},
	},
	warning: {
		filled: {
			color: basicColors.white,
			bg: dark.yellow3,
			border: dark.yellow3,
			disabled: {
				bg: dark.grey7,
				border: dark.grey7,
				color: dark.grey3,
			},
			hover: {
				bg: dark.yellow2,
				border: dark.yellow2,
			},
			active: {
				bg: dark.yellow4,
				border: dark.yellow4,
			},
		},
		transparent: {
			color: dark.yellow3,
			bg: basicColors.transparent,
			border: dark.yellow3,
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey7,
				border: dark.grey7,
			},
			hover: {
				bg: dark.yellow2,
				color: basicColors.white,
			},
			active: {
				bg: dark.yellow4,
				border: dark.yellow5,
				color: basicColors.white,
			},
		},
		link: {
			color: dark.yellow3,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.yellow1,
			},
			active: {
				color: dark.yellow5,
			},
		},
		text: {
			color: dark.yellow3,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.yellow1,
			},
			active: {
				color: dark.yellow5,
			},
		},
		icon: {
			color: dark.yellow3,
			disabled: {
				color: dark.grey8,
			},
			hover: {
				color: dark.yellow1,
			},
			active: {
				color: dark.yellow5,
			},
		},
		flag: {
			color: dark.grey5,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.yellow1,
			},
			active: {
				bg: dark.grey9,
				color: dark.yellow5,
			},
			disabled: {
				color: dark.grey8,
				bg: dark.grey6,
			},
			selected: {
				bg: dark.grey8,
				color: dark.yellow3,
			},
		},
	},
	danger: {
		filled: {
			color: basicColors.white,
			bg: dark.red5,
			border: dark.red5,
			disabled: {
				bg: dark.grey7,
				border: dark.grey7,
				color: dark.grey3,
			},
			hover: {
				bg: dark.red4,
				border: dark.red4,
			},
			active: {
				bg: dark.red6,
				border: dark.red6,
			},
		},
		transparent: {
			color: dark.red6,
			bg: basicColors.transparent,
			border: dark.red6,
			disabled: {
				bg: basicColors.transparent,
				color: dark.grey7,
				border: dark.grey7,
			},
			hover: {
				bg: dark.red6,
				color: basicColors.white,
			},
			active: {
				bg: dark.red7,
				border: dark.red8,
				color: basicColors.white,
			},
		},
		link: {
			color: dark.red5,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.red3,
			},
			active: {
				color: dark.red7,
			},
		},
		text: {
			color: dark.red5,
			disabled: {
				color: dark.grey7,
			},
			hover: {
				color: dark.red3,
			},
			active: {
				color: dark.red7,
			},
		},
		icon: {
			color: dark.red5,
			disabled: {
				color: dark.grey8,
			},
			hover: {
				color: dark.red3,
			},
			active: {
				color: dark.red7,
			},
		},
		flag: {
			color: dark.grey5,
			bg: basicColors.transparent,
			hover: {
				bg: dark.grey8,
				color: dark.red3,
			},
			active: {
				bg: dark.grey9,
				color: dark.red7,
			},
			disabled: {
				color: dark.grey8,
				bg: dark.grey6,
			},
			selected: {
				bg: dark.grey8,
				color: dark.red5,
			},
		},
	},
};
