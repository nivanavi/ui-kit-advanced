type AllButtonVariants = {
	filled: {
		color: string;
		bg: string;
		border: string;
		disabled: {
			bg: string;
			border: string;
			color: string;
		};
		hover: {
			bg: string;
			border: string;
		};
		active: {
			bg: string;
			border: string;
		};
	};
	transparent: {
		color: string;
		bg: string;
		border: string;
		hover: {
			bg: string;
			color: string;
		};
		active: {
			bg: string;
			border: string;
			color: string;
		};
		disabled: {
			bg: string;
			color: string;
			border: string;
		};
	};
	link: {
		color: string;
		hover: {
			color: string;
		};
		active: {
			color: string;
		};
		disabled: {
			color: string;
		};
	};
	text: {
		color: string;
		hover: {
			color: string;
		};
		active: {
			color: string;
		};
		disabled: {
			color: string;
		};
	};
	icon: {
		color: string;
		hover: {
			color: string;
		};
		active: {
			color: string;
		};
		disabled: {
			color: string;
		};
	};
	// diff
	flag: {
		color: string;
		bg: string;
		hover: {
			bg: string;
			color: string;
		};
		active: {
			bg: string;
			color: string;
		};
		disabled: {
			color: string;
			bg: string;
		};
		selected: {
			bg: string;
			color: string;
		};
	};
};

export type ButtonThemeColors = {
	common: AllButtonVariants;
	default: AllButtonVariants;
	success: AllButtonVariants;
	info: AllButtonVariants;
	warning: AllButtonVariants;
	danger: AllButtonVariants;
};
