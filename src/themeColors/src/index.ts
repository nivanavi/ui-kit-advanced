export { THEME_LIGHT_COLORS, THEME_DARK_COLORS } from './themeColors';
export type { ThemeColorsType } from './types';
