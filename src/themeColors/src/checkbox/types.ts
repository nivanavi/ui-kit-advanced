type CheckboxCondition = {
	border: string;
	bg: string;
	color: string;
};

export type CheckboxThemeColors = {
	color: string;
	hoverColor: string;
	activeColor: string;
	disabledColor: string;
	invalidColor: string;
	requiredColor: string;
	box: {
		hover: CheckboxCondition;
		active: CheckboxCondition;
		checked: CheckboxCondition;
		disabled: CheckboxCondition;
		invalid: CheckboxCondition;
	} & CheckboxCondition;
};
