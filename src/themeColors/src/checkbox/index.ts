import { CheckboxThemeColors } from './types';
import { basicColors, dark, light } from '../colors';

export const checkboxLightColors: CheckboxThemeColors = {
	color: light.grey8,
	hoverColor: light.grey6,
	activeColor: light.grey9,
	disabledColor: light.grey5,
	invalidColor: light.red5,
	requiredColor: light.red5,
	box: {
		border: light.blue5,
		bg: basicColors.transparent,
		color: basicColors.transparent,
		hover: {
			border: light.blue5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
		active: {
			border: light.blue5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
		checked: {
			border: light.blue5,
			bg: light.blue5,
			color: basicColors.white,
		},
		disabled: {
			border: light.grey2,
			bg: light.grey2,
			color: light.grey5,
		},
		invalid: {
			border: light.red5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
	},
};

export const checkboxDarkColors: CheckboxThemeColors = {
	color: dark.grey1,
	hoverColor: dark.grey0,
	activeColor: dark.grey2,
	disabledColor: dark.grey6,
	invalidColor: dark.red5,
	requiredColor: dark.red5,
	box: {
		border: dark.blue5,
		bg: basicColors.transparent,
		color: basicColors.transparent,
		hover: {
			border: light.blue5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
		active: {
			border: light.blue5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
		checked: {
			border: dark.blue5,
			bg: dark.blue5,
			color: basicColors.white,
		},
		disabled: {
			border: dark.grey6,
			bg: dark.grey6,
			color: dark.grey3,
		},
		invalid: {
			border: dark.red5,
			bg: basicColors.transparent,
			color: basicColors.transparent,
		},
	},
};
