export type MessageThemeColors = {
	errorColor: string;
	hintColor: string;
	successColor: string;
	defaultColor: string;
};
