import { MessageThemeColors } from './types';
import { dark, light, basicColors } from '../colors';

export const messageLightColors: MessageThemeColors = {
	errorColor: light.red5,
	hintColor: light.grey5,
	successColor: light.green5,
	defaultColor: basicColors.black,
};

export const messageDarkColors: MessageThemeColors = {
	errorColor: dark.red4,
	hintColor: light.grey5,
	successColor: dark.green4,
	defaultColor: basicColors.black,
};
