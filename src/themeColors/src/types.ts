import { InputThemeColors } from './input/types';
import { ButtonThemeColors } from './button/types';
import { AccordionThemeColors } from './accordion/types';
import { CheckboxThemeColors } from './checkbox/types';
import { TextareaThemeColors } from './textarea/types';
import { RadioThemeColors } from './radio/types';
import { UploadFilesThemeColors } from './uploadFiles/types';
import { MessageThemeColors } from './message/types';
import { SelectOptionsThemeColors } from './selectOptions/types';

export type ThemeColorsType = {
	input: InputThemeColors;
	button: ButtonThemeColors;
	accordion: AccordionThemeColors;
	checkbox: CheckboxThemeColors;
	textarea: TextareaThemeColors;
	radio: RadioThemeColors;
	uploadFiles: UploadFilesThemeColors;
	message: MessageThemeColors;
	selectOptions: SelectOptionsThemeColors;
};
