export type AccordionThemeColors = {
	border: string;
	borderOpen: string;
	icon: {
		color: string;
		hoverColor: string;
	};
};
