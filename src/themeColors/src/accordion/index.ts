import { AccordionThemeColors } from './types';
import { dark, light } from '../colors';

export const accordionLightColors: AccordionThemeColors = {
	border: light.grey4,
	borderOpen: light.grey6,
	icon: {
		color: light.grey5,
		hoverColor: light.grey3,
	},
};

export const accordionDarkColors: AccordionThemeColors = {
	border: dark.grey5,
	borderOpen: dark.grey3,
	icon: {
		color: dark.grey4,
		hoverColor: dark.grey2,
	},
};
