import { SelectOption, SelectOptionsProps } from '@ui-kit-advanced/select-options';
import { InputProps } from '@ui-kit-advanced/input/dist/types';

export type SelectProps<T> = {
	defaultValue?: SelectOption<T>;
	onChangeInput?: (value: string) => void;
	onChange?: (value: SelectOption<T> | null) => void;
	isSearchAvailable?: boolean;
	isClearable?: boolean;
} & Omit<SelectOptionsProps<T>, 'onChange'> &
	Pick<
		InputProps,
		'isInvalid' | 'isLoading' | 'isDisabled' | 'iconBefore' | 'iconAfter' | 'isRequired' | 'placeholder'
	>;
