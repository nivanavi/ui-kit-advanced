import styled from 'styled-components';
import React from 'react';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';

export const StyledSelectWrapper = styled.div`
	position: relative;

	.optionsShow {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: unset;
		opacity: 1;
	}

	.optionsHide {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: none;
		opacity: 0;
	}
`;

export const StyledSelectEvents = styled.div`
	height: 100%;
	display: flex;
	align-items: center;

	> .iconButton {
		margin: 0.5rem 1rem 0 0;
	}
`;

export const StyledSelectArrowIcon = styled.div`
	height: 100%;
	display: flex;
	align-items: center;
`;
