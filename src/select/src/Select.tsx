import React from 'react';
import { SelectOption, SelectOptions } from '@ui-kit-advanced/select-options';
import { Input } from '@ui-kit-advanced/input';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { CSSTransition } from 'react-transition-group';
import { Button } from '@ui-kit-advanced/button';
import { CloseIcon, SelectArrowIcon } from '@ui-kit-advanced/icons';
import { ClickAway, useSwitch } from '@ui-kit-advanced/hooks';
import { ConditionalRender } from '@ui-kit-advanced/hooks/dist/components/conditionalRender';
import { SelectProps } from './types';
import { StyledSelectArrowIcon, StyledSelectEvents, StyledSelectWrapper } from './styles';

// todo разобраться с анимацией скрытия

const SelectComponent = <T extends { id: string }>(props: SelectProps<T>): JSX.Element => {
	const {
		options,
		value,
		emptyMessage,
		defaultValue,
		dropDownHelpMessage,
		isClearable,
		isSearchAvailable,
		onChange,
		onChangeInput,
		isLoading,
		isDisabled,
		...rest
	} = props;
	const [stateIsOpen, switchIsOpen] = useSwitch(false);
	const [stateInputValue, setInputValue] = React.useState<string>('');
	const [stateValue, setValue] = React.useState<SelectOption<T> | null>(defaultValue || null);

	const refIsTouched = React.useRef<boolean>(false);
	const isShowClear: boolean = !!stateValue && Boolean(isClearable);

	const backInputValue = React.useCallback((): void => {
		refIsTouched.current = false;
		if (isSearchAvailable && stateInputValue !== stateValue?.label) setInputValue(stateValue?.label || '');
	}, [isSearchAvailable, stateInputValue, stateValue?.label]);

	React.useEffect(() => {
		if (value === undefined) return;
		setValue(value);
		if (isSearchAvailable) setInputValue(value?.label || '');
	}, [value, isSearchAvailable]);

	React.useEffect(() => {
		if (!stateIsOpen) backInputValue();
	}, [backInputValue, stateIsOpen]);

	const clearSelectHandler = React.useCallback(() => {
		onChange?.(null);
		onChangeInput?.('');
		if (value !== undefined) return;
		setValue(null);
		if (isSearchAvailable) setInputValue('');
	}, [onChange, onChangeInput, value, isSearchAvailable]);

	const onChangeHandler = React.useCallback(
		(currentValue: SelectOption<T> | null) => {
			onChange?.(currentValue);
			onChangeInput?.(currentValue?.label || '');
			if (value !== undefined || stateValue?.value === currentValue?.value) return;
			setValue(currentValue);
			if (isSearchAvailable) setInputValue(currentValue?.label || '');
		},
		[onChange, onChangeInput, value, stateValue?.value, isSearchAvailable]
	);

	const inputChangeHandler = React.useCallback(
		({ target: { value: currentInputValue } }: React.ChangeEvent<HTMLInputElement>) => {
			refIsTouched.current = true;
			if (!stateIsOpen) switchIsOpen(true);
			if (!currentInputValue) onChangeHandler(null);
			onChangeInput?.(currentInputValue);
			setInputValue(currentInputValue);
		},
		[onChangeHandler, onChangeInput, switchIsOpen, stateIsOpen]
	);

	const selectOptionHandler = React.useCallback(
		(option: SelectOption<T>) => {
			onChangeHandler(option);
			switchIsOpen(false);
		},
		[onChangeHandler, switchIsOpen]
	);

	const filterOptions: SelectOption<T>[] =
		isSearchAvailable && refIsTouched.current
			? options.filter(({ label }) => String(label).toLowerCase().includes(stateInputValue.toLowerCase()))
			: options;

	return (
		<ClickAway
			isListenEscape={stateIsOpen}
			onClickAway={(): void => {
				switchIsOpen(false);
			}}
		>
			<StyledSelectWrapper className='selectWrapper'>
				<Input
					{...rest}
					readOnly={!isSearchAvailable}
					onClick={(): void => switchIsOpen()}
					isDisabled={isDisabled}
					isLoading={isLoading}
					onChange={inputChangeHandler}
					value={isSearchAvailable ? stateInputValue : stateValue?.label || ''}
					iconAfter={
						<StyledSelectEvents>
							<ConditionalRender isRender={isShowClear}>
								<Button
									isDisabled={isDisabled}
									appearance='icon'
									onClick={(): void => clearSelectHandler()}
									icon={<CloseIcon />}
								/>
							</ConditionalRender>
							<StyledSelectArrowIcon onClick={(): void => switchIsOpen()}>
								<SelectArrowIcon direction={stateIsOpen ? 'top' : 'down'} />
							</StyledSelectArrowIcon>
						</StyledSelectEvents>
					}
				/>
				<CSSTransition
					in={stateIsOpen}
					timeout={ANIMATION_SEC * 1000}
					mountOnEnter
					unmountOnExit
					classNames={{
						enterActive: 'optionsShow',
						enterDone: 'optionsShow',
						exitActive: 'optionsHide',
						exitDone: 'optionsHide',
					}}
				>
					<SelectOptions
						isLoading={isLoading}
						options={filterOptions}
						emptyMessage={emptyMessage}
						value={stateValue}
						onChange={selectOptionHandler}
						dropDownHelpMessage={dropDownHelpMessage}
					/>
				</CSSTransition>
			</StyledSelectWrapper>
		</ClickAway>
	);
};

export const Select = React.memo(SelectComponent) as typeof SelectComponent;
