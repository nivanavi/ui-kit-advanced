import styled from 'styled-components';
import React from 'react';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { THEME_LIGHT_COLORS } from '@ui-kit-advanced/theme-colors';

export const StyledSelectWrapper = styled.div`
	position: relative;

	.optionsShow {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: unset;
		opacity: 1;
	}

	.optionsHide {
		transition: opacity ${ANIMATION_SEC}s;
		pointer-events: none;
		opacity: 0;
	}
`;

export const StyledSelectEvents = styled.div`
	height: 100%;
	display: flex;
	align-items: center;

	> .iconButton {
		margin: 0.5rem 1rem 0 0;
	}
`;

export const StyledSelectArrowIcon = styled.div`
	height: 100%;
	display: flex;
	align-items: center;
`;

export const StyledPickedItemsWrapper = styled.div`
	border: 2px solid ${({ theme }) => theme.input.border};
	box-sizing: border-box;
	margin-bottom: -2px;
	padding: 0.5rem;
	border-radius: 5px;
`;

export const StyledPickedItem = styled.div`
	display: inline-flex;
	align-items: center;
	.iconButton {
		margin-left: 1rem;
	}
	margin-right: 0.5rem;
	margin-bottom: 0.5rem;
	padding: 0.5rem;
	border: 2px solid ${({ theme }) => theme.select.border};
	border-radius: 5px;
`;

StyledPickedItemsWrapper.defaultProps = { theme: THEME_LIGHT_COLORS };
StyledPickedItem.defaultProps = { theme: THEME_LIGHT_COLORS };
