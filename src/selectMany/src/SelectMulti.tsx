import React from 'react';
import { SelectOption, SelectOptions } from '@ui-kit-advanced/select-options';
import { Input } from '@ui-kit-advanced/input';
import { ClickAway, useSwitch } from '@ui-kit-advanced/hooks';
import { ANIMATION_SEC } from '@ui-kit-advanced/consts';
import { CSSTransition } from 'react-transition-group';
import { Button } from '@ui-kit-advanced/button';
import { CloseIcon, SelectArrowIcon, TrashIcon } from '@ui-kit-advanced/icons';
import { Text } from '@ui-kit-advanced/text';
import {
	StyledPickedItem,
	StyledPickedItemsWrapper,
	StyledSelectArrowIcon,
	StyledSelectEvents,
	StyledSelectWrapper,
} from './styles';
import { SelectMultiProps } from './types';

// todo разобраться с анимацией скрытия
// todo добавить анимацию к добавлению и удалению элементов

const SelectMultiComponent = <T extends { id: string }>(props: SelectMultiProps<T>): JSX.Element => {
	const {
		options,
		value,
		emptyMessage,
		defaultValue,
		dropDownHelpMessage,
		isClearable,
		isSearchAvailable,
		onChange,
		isLoading,
		isDisabled,
		...rest
	} = props;
	const [stateIsOpen, switchIsOpen] = useSwitch(false);
	const [stateInputValue, setInputValue] = React.useState<string>('');
	const [stateValue, setValue] = React.useState<SelectOption<T>[] | null>(defaultValue || null);

	const refIsTouched = React.useRef<boolean>(false);
	const isShowClear: boolean = Array.isArray(stateValue) && Boolean(stateValue.length) && Boolean(isClearable);
	const isShowSelected: boolean = Array.isArray(stateValue) && Boolean(stateValue.length);

	const backInputValue = React.useCallback((): void => {
		refIsTouched.current = false;
	}, []);

	React.useEffect(() => {
		if (value === undefined) return;
		setValue(value);
	}, [value, isSearchAvailable]);

	React.useEffect(() => {
		if (!stateIsOpen) backInputValue();
	}, [backInputValue, stateIsOpen]);

	const clearSelectHandler = React.useCallback(() => {
		onChange?.(null);
		if (value !== undefined) return;
		setValue(null);
		if (isSearchAvailable) setInputValue('');
	}, [onChange, value, isSearchAvailable]);

	const onChangeHandler = React.useCallback(
		(currentValue: SelectOption<T>[] | null) => {
			onChange?.(currentValue);
			if (value !== undefined) return;
			setValue(currentValue);
			if (isSearchAvailable) setInputValue('');
		},
		[onChange, value, isSearchAvailable]
	);

	const inputChangeHandler = React.useCallback(
		({ target: { value: currentInputValue } }: React.ChangeEvent<HTMLInputElement>) => {
			refIsTouched.current = true;
			if (!stateIsOpen) switchIsOpen(true);
			if (!currentInputValue) onChangeHandler(null);
			setInputValue(currentInputValue);
		},
		[onChangeHandler, switchIsOpen, stateIsOpen]
	);

	const selectOptionHandler = React.useCallback(
		(option: SelectOption<T>) => {
			onChangeHandler([...(stateValue || []), option]);
			switchIsOpen(false);
		},
		[onChangeHandler, switchIsOpen, stateValue]
	);

	const deleteOptionHandler = React.useCallback(
		(option: SelectOption<T>) => {
			onChangeHandler((stateValue || []).filter(stateOption => stateOption.value.id !== option.value.id));
			switchIsOpen(false);
		},
		[onChangeHandler, stateValue, switchIsOpen]
	);

	const filterOptions: SelectOption<T>[] =
		isSearchAvailable && refIsTouched.current
			? options.filter(({ label }) => String(label).toLowerCase().includes(stateInputValue.toLowerCase()))
			: options;

	return (
		<ClickAway isListenEscape={stateIsOpen} onClickAway={(): void => switchIsOpen(false)}>
			<StyledSelectWrapper className='selectWrapper'>
				{isShowSelected ? (
					<StyledPickedItemsWrapper>
						{(stateValue || []).map(option => (
							<StyledPickedItem key={`picked-item-${option.value.id}`}>
								<Text>{option.label}</Text>
								<Button onClick={(): void => deleteOptionHandler(option)} appearance='icon' icon={<TrashIcon />} />
							</StyledPickedItem>
						))}
					</StyledPickedItemsWrapper>
				) : null}
				<Input
					{...rest}
					readOnly={!isSearchAvailable}
					onClick={(): void => switchIsOpen()}
					isDisabled={isDisabled}
					isLoading={isLoading}
					onChange={inputChangeHandler}
					value={stateInputValue}
					iconAfter={
						<StyledSelectEvents>
							{isShowClear && (
								<Button
									isDisabled={isDisabled}
									appearance='icon'
									onClick={(): void => clearSelectHandler()}
									icon={<CloseIcon />}
								/>
							)}
							<StyledSelectArrowIcon onClick={(): void => switchIsOpen()}>
								<SelectArrowIcon direction={stateIsOpen ? 'top' : 'down'} />
							</StyledSelectArrowIcon>
						</StyledSelectEvents>
					}
				/>
				<CSSTransition
					in={stateIsOpen}
					timeout={ANIMATION_SEC * 1000}
					mountOnEnter
					unmountOnExit
					classNames={{
						enterActive: 'optionsShow',
						enterDone: 'optionsShow',
						exitActive: 'optionsHide',
						exitDone: 'optionsHide',
					}}
				>
					<SelectOptions
						isLoading={isLoading}
						options={filterOptions}
						emptyMessage={emptyMessage}
						onChange={selectOptionHandler}
						dropDownHelpMessage={dropDownHelpMessage}
					/>
				</CSSTransition>
			</StyledSelectWrapper>
		</ClickAway>
	);
};

export const SelectMulti = React.memo(SelectMultiComponent) as typeof SelectMultiComponent;
