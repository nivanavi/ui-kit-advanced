import { SelectOption, SelectOptionsProps } from '@ui-kit-advanced/select-options';
import { InputProps } from '@ui-kit-advanced/input/dist/types';

export type SelectMultiProps<T> = {
	value?: SelectOption<T>[];
	defaultValue?: SelectOption<T>[];
	onChange?: (value: SelectOption<T>[] | null) => void;
	isSearchAvailable?: boolean;
	isClearable?: boolean;
} & Omit<SelectOptionsProps<T>, 'onChange' | 'value'> &
	Pick<InputProps, 'isInvalid' | 'isLoading' | 'isDisabled' | 'iconBefore' | 'iconAfter' | 'isRequired'>;
